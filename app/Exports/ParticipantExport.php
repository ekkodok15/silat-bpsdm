<?php

namespace App\Exports;

use App\Models\TrainingOption;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ParticipantExport implements
    FromCollection,
    WithHeadings,
    WithMapping,
    ShouldAutoSize
{
    use Exportable;

    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function headings(): array
    {
        return[
            'nama',
            'no_identitas',
            'jenis_kelamin',
            'agama',
            'tempat_lahir',
            'tgl_lahir',
            'email',
            'no_hp / telp_kantor',
            'jenis_peserta',
            'gol',
            'pangkat',
            'jabatan',
            'pola_penyelenggaraan',
            'sumber_anggaran',
            'Instansi',
            'Alamat Instansi',
            'Unit Kerja',
        ];
    }

    public function collection()
    {
        return TrainingOption::with(['takeTrainingPlan', 'takeClass', 'takeUserBiodata'])->where('training_plan_id','=', $this->id)->where('status','1')->get();

    }

    public function map($trainingoption): array
    {
        return [
            (String) $trainingoption->takeUserBiodata->name,
            (String) $trainingoption->nip_user,
            (String) $trainingoption->takeUserBiodata->takeGender->gender,
            (String) $trainingoption->takeUserBiodata->takeReligion->religion,
            (String) $trainingoption->takeUserBiodata->place_of_birth,
            (String) date('d-m-Y', strtotime($trainingoption->takeUserBiodata->birth_day)),
            (String) $trainingoption->takeUserBiodata->takeUser->email,
            (String) $trainingoption->takeUserBiodata->phone,
            (String) $trainingoption->takeUserBiodata->takeParticipantType->participant_type,
            (String) $trainingoption->takeUserBiodata->takeGroup->group,
            (String) $trainingoption->takeUserBiodata->takeRank->rank,
            (String) $trainingoption->takeUserBiodata->department,
            (String) $trainingoption->takeTrainingPlan->takeImplementationPattern->implementation_pattern,
            (String) $trainingoption->takeTrainingPlan->takeBudgetSource->budget_source,
            (String) $trainingoption->takeUserBiodata->takeAgency->agency,
            (String) $trainingoption->takeUserBiodata->work_unit_address,
            (String) $trainingoption->takeUserBiodata->takeWorkUnit->work_unit,
        ];
    }
}
