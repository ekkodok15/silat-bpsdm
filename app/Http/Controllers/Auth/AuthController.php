<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\UserBiodata;
use App\Models\User;
use Auth;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.peserta.login');
    }

    public function regist()
    {
        return view('auth.peserta.regist');
    }

    public function loginAdmin()
    {
        return view('auth.admin.login');
    }

    public function loginProses(Request $request)
    {
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect('/dashboard');
        }elseif (Auth::guard('user')->attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect('/home');
        }
            return redirect('/login')->with(['error' => "Harap cek dan periksa kembali email dan password anda!"]);
    }

    public function registProses(Request $request)
    {
        $inputBiodataUser = UserBiodata::create([
            'nip'  => $request->nip,
            'name' => $request->name,
        ]);

        if ($inputBiodataUser == true) {
            $inputUser = new User;
            $inputUser->email = $request->email;
            $inputUser->level_user_id = 1;
            $inputUser->status = "0";
            $inputUser->password = bcrypt($request->password);
            $inputUser->nip_user = $request->nip;
            $inputUser->remember_token = Str::random(60);
            $inputUser->save();

            if ($inputUser == true) {
                return redirect('/login')->with(['success' => "Selamat anda berhasil mendaftarkan diri anda !"]);
            }else {
                return redirect('/login')->with(['error' => "Harap melakukan pengecekan kembali pada data yang di inputkan!"]);
            }
        }else {
            return redirect('/login')->with(['error' => "Harap untuk menginputkan nama dan nip dengan baik dan benar!"]);
        }
    }

    public function logout()
    {
        if (Auth::guard('admin')->check()) {
            Auth::guard('admin')->logout();
            return redirect('/login/admin');
        }elseif (Auth::guard('user')->check()) {
            Auth::guard('user')->logout();
            return redirect('/');
        }
    }
}
