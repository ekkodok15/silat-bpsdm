<?php

namespace App\Http\Controllers\User;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Models\ParticipantType;
Use App\Models\TrainingPlan;
use App\Models\TrainingOption;
use Illuminate\Http\Request;
use App\Models\UserBiodata;
use App\Models\Religion;
use App\Models\WorkUnit;
use App\Models\Gender;
use App\Models\Agency;
use App\Models\Group;
use App\Models\User;
use App\Models\Rank;

class HomeController extends Controller
{
    public function index()
    {
        $user = UserBiodata::where('nip',Auth::User()->nip_user)->first();
        if (Auth::User()->level_user_id == 1) {
            return redirect('/profile/pilih-asn');
        }else{
            if($user->photo == null){
                return redirect()->route('upload.photo');
            }else{
                if ($user->place_of_birth == null && $user->work_unit_id == null) {
                    return redirect()->route('profile');
                }else{
                    $trainingoption = TrainingOption::where('nip_user',"=",Auth::User()->nip_user)->whereIn('status',['0','1'])->orderBy('id_training_option','DESC')->get()->toArray();
                    // dd($trainingoption);
                    $training = TrainingPlan::whereYear( 'regist_start_date',  date('Y') )->whereMonth('regist_start_date',  date('m'))->get();
                    return view('user.home', compact('training','trainingoption'));
                }
            }
        }
    }

    public function pilihAsn()
    {
        if (Auth::User()->level_user_id == 1) {
            return view('user.pilih-asn');
        }else{
            return redirect('/home');
        }
    }

    public function updateAsn(Request $request)
    {
        if (!empty(Auth::User())) {
            $status = User::where('id_user', Auth::User()->id_user)
                        ->update(['level_user_id' => $request->status]);
            if ($status == true) {
                return redirect('/profile');
            }else {
                return redirect('/profile/pilih-asn');;
            }
        }else{
            return redirect('/login');
        }

    }

    public function profile()
    {
        if (Auth::User()->level_user_id == 1) {
            return redirect('/profile/pilih-asn');
        }else{
            $nip            = Auth::User()->nip_user;
            $user           = UserBiodata::find($nip);
            $participantype = ParticipantType::all();
            $religion       = Religion::all();
            $group          = Group::all();
            $gender         = Gender::all();
            $agency         = Agency::all();
            $rank           = Rank::all();
            $workunit       = WorkUnit::all();
            return view('user.profile',compact('user','participantype','religion','group','gender','agency','rank','workunit'));
        }
    }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'phone' => 'required',
            'jenis_pegawai' => 'required',
            'pangkat' => 'required',
            'golongan' => 'required',
            'instansi' => 'required',
            'unit_kerja' => 'required',
        ]);

        if (!empty(Auth::User())) {
            if (Auth::User()->level_user_id != 1) {
                $updatebiodata                          = UserBiodata::find(Auth::User()->nip_user);
                $updatebiodata->name                    = $request->name;
                $updatebiodata->gender_id               = $request->jenis_kelamin;
                $updatebiodata->religion_id             = $request->agama;
                $updatebiodata->place_of_birth          = $request->tempat_lahir;
                $updatebiodata->birth_day               = $request->tanggal_lahir;
                $updatebiodata->phone                   = $request->phone;
                $updatebiodata->participant_type_id     = $request->jenis_pegawai;
                $updatebiodata->group_id                = $request->pangkat;
                $updatebiodata->rank_id                 = $request->golongan;
                $updatebiodata->agency_id               = $request->instansi;
                $updatebiodata->work_unit_id            = $request->unit_kerja;
                $updatebiodata->department              = $request->jabatan;
                $updatebiodata->work_unit_address       = $request->alamat_unit_kerja;
                $updatebiodata->save();

                if ($updatebiodata == true) {
                    $pict = UserBiodata::find(Auth::User()->nip_user);
                    if ($pict->photo == null) {
                        return redirect('/profile/upload-foto');
                    }else{
                        return redirect('/home');
                    }
                }else {
                    return redirect('/profile');;
                }
            }else{
                return redirect('/profile/pilih-asn');;
            }
        }else{
            return redirect('/login');
        }

    }

    public function uploadPhoto()
    {
        if (Auth::User()->level_user_id == 1) {
            return redirect('/profile/pilih-asn');
        }else{
            return view('user.photo');
        }
    }

    public function updatePhoto(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg|max:1024',
        ]);

        $user = UserBiodata::where('nip',Auth::User()->nip_user)->first();
        if (!empty(Auth::User())) {
            if (Auth::User()->level_user_id != 1) {
                if ($user->photo == null) {
                    if($request->hasFile('image')){
                        $username       = UserBiodata::find(Auth::User()->nip_user);
                        $resorce        = $request->file('image');
                        $name           = $username->name.'-FOTO_PROFILE'. '.' . $resorce->extension();
                        $resorce->move(public_path() ."/public/images", $name);
                        $updatebiodata                          = UserBiodata::find(Auth::User()->nip_user);
                        $updatebiodata->photo                   = $name;
                        $updatebiodata->save();

                        if ($updatebiodata == true) {
                            return redirect('/home');
                        }else {
                            return redirect('/profile/upload-foto');
                        }
                    }else{
                        return redirect('/profile/upload-foto');
                    }
                }else{
                    $image_path = public_path("public/images/".$user->photo);

                    if(file_exists($image_path)){
                        $deleted = File::delete( $image_path);
                    }else{
                        echo "no file here";
                    }

                    if($deleted == true){
                        if($request->hasFile('image')){
                            $username       = UserBiodata::find(Auth::User()->nip_user);
                            $resorce        = $request->file('image');
                            $name           = $username->name.'-FOTO-'.time() . '.' . $resorce->extension();
                            $resorce->move(public_path() ."/public/images", $name);
                            $updatebiodata                          = UserBiodata::find(Auth::User()->nip_user);
                            $updatebiodata->photo                   = $name;
                            $updatebiodata->save();

                            if ($updatebiodata == true) {
                                return redirect('/home');
                            }else {
                                return redirect('/profile/upload-foto');;
                            }
                        }else{
                            return redirect('/profile/upload-foto');
                        }
                    }else {
                        return redirect('/profile/upload-foto');
                    }
                }
            }else{
                return redirect('/profile/pilih-asn');;
            }
        }else{
            return redirect('/login');
        }

    }
}
