<?php

namespace App\Http\Controllers\User;

use Illuminate\Database\Eloquent\Builder;
use App\Http\Controllers\Controller;
use App\Models\TrainingClass;
use App\Models\TrainingOption;
use Illuminate\Http\Request;
Use App\Models\TrainingPlan;
use App\Models\UserBiodata;
use Auth;

class DiklatController extends Controller
{
    public function index()
    {
        $user = UserBiodata::where('nip',Auth::User()->nip_user)->first();
        if (Auth::User()->level_user_id == 1) {
            return redirect('/profile/pilih-asn');
        }else{
            if($user->photo == null){
                return redirect()->route('upload.photo');
            }else{
                if ($user->place_of_birth == null && $user->work_unit_id == null) {
                    return redirect()->route('profile');
                }else{
                    $trainingoption = TrainingOption::where('nip_user',"=",Auth::User()->nip_user)->whereIn('status',['0','1'])->orderBy('id_training_option','DESC')->get()->toArray();
                    // dd($trainingoption);
                    $training = TrainingPlan::whereYear( 'regist_start_date',  date('Y') )->whereMonth('regist_start_date',  date('m'))->get();
                    return view('user.all-diklat', compact('training','trainingoption'));
                }
            }
        }

    }

    public function manajerial()
    {
        $user = UserBiodata::where('nip',Auth::User()->nip_user)->first();
        if (Auth::User()->level_user_id == 1) {
            return redirect('/profile/pilih-asn');
        }else{
            if($user->photo == null){
                return redirect()->route('upload.photo');
            }else{
                if ($user->place_of_birth == null && $user->work_unit_id == null) {
                    return redirect()->route('profile');
                }else{
                    $trainingoption = TrainingOption::where('nip_user',"=",Auth::User()->nip_user)->whereIn('status',['0','1'])->orderBy('id_training_option','DESC')->get()->toArray();
                    // dd($trainingoption);
                    $training = TrainingPlan::whereYear( 'approved_start_date',  date('Y') )
                                            ->whereHas('takeTraining', function (Builder $query) {
                                            $query->where('type_of_training_id', '=', '1');
                                            })->get();
                    return view('user.diklat-manajerial', compact('training','trainingoption'));
                }
            }
        }
    }

    public function fungsional()
    {
        $user = UserBiodata::where('nip',Auth::User()->nip_user)->first();
        if (Auth::User()->level_user_id == 1) {
            return redirect('/profile/pilih-asn');
        }else{
            if($user->photo == null){
                return redirect()->route('upload.photo');
            }else{
                if ($user->place_of_birth == null && $user->work_unit_id == null) {
                    return redirect()->route('profile');
                }else{
                    $trainingoption = TrainingOption::where('nip_user',"=",Auth::User()->nip_user)->whereIn('status',['0','1'])->orderBy('id_training_option','DESC')->get()->toArray();
                    // dd($trainingoption);
                    $training = TrainingPlan::whereYear( 'approved_start_date',  date('Y') )
                                            ->whereHas('takeTraining', function (Builder $query) {
                                            $query->where('type_of_training_id', '=', '3');
                                            })->get();
                    return view('user.diklat-fungsional', compact('training','trainingoption'));
                }
            }
        }
    }

    public function teknis()
    {
        $user = UserBiodata::where('nip',Auth::User()->nip_user)->first();
        if (Auth::User()->level_user_id == 1) {
            return redirect('/profile/pilih-asn');
        }else{
            if($user->photo == null){
                return redirect()->route('upload.photo');
            }else{
                if ($user->place_of_birth == null && $user->work_unit_id == null) {
                    return redirect()->route('profile');
                }else{
                    $trainingoption = TrainingOption::where('nip_user',"=",Auth::User()->nip_user)->whereIn('status',['0','1'])->orderBy('id_training_option','DESC')->get()->toArray();
                    // dd($trainingoption);
                    $training = TrainingPlan::whereYear( 'approved_start_date',  date('Y') )
                                            ->whereHas('takeTraining', function (Builder $query) {
                                            $query->where('type_of_training_id', '=', '2');
                                            })->get();
                    return view('user.diklat-teknis', compact('training','trainingoption'));
                }
            }
        }
    }

    public function history()
    {
        $training = TrainingOption::with(['takeTrainingPlan'])->where( 'nip_user','=',  Auth::User()->nip_user )->get();
        return view('user.diklat-history', compact('training'));
    }

    public function choose($id_taining_plan)
    {
        $class      = TrainingClass:: where('training_plan_id','=',$id_taining_plan)->get();
        $training   = TrainingPlan::where('id_training_plan','=',$id_taining_plan)->first();
        return view('user.diklat-choose', compact('training','class'));
    }

    public function proses(Request $request)
    {
        $request->validate([
            'spt' => 'required|mimes:PDF,pdf|max:2048',
            'ruko' => 'required|mimes:PDF,pdf|max:2048'
        ]);

        if (!empty(Auth::User())) {
                if($request->hasFile('spt','ruko')){
                    $username                               = UserBiodata::find(Auth::User()->nip_user);
                    $spt                                    = $request->file('spt');
                    $namespt                                = $username->name.'-SPT-'.time() . '.' . $spt->extension();
                    $spt->move(public_path() ."/public/spt", $namespt);

                    $ruko                                   = $request->file('ruko');
                    $nameruko                               = $username->name.'-RUKO-'.time() . '.' . $ruko->extension();
                    $ruko->move(public_path() ."/public/ruko", $nameruko);

                    $pilihdiklat                          = new TrainingOption;
                    $pilihdiklat->nip_user                = Auth::User()->nip_user;
                    $pilihdiklat->training_plan_id        = $request->id_training_plan;
                    $pilihdiklat->class_id                = $request->class;
                    $pilihdiklat->spt                     = $namespt ;
                    $pilihdiklat->ruko                    = $nameruko;
                    $pilihdiklat->status                  = "0";
                    $pilihdiklat->save();

                    if ($pilihdiklat == true) {
                        return redirect('/home')->with(['success' => "Selamat anda Telah bergabung dengan diklat yang anda pilih tunggu info berikutnya!"]);
                    }else {
                        return redirect('/home')->with(['error' => "Harap cek dan periksa kembali file yang anda upload, data tidak dapat ,asuk ke server!"]);
                    }
                }else{
                    return redirect('/home')->with(['error' => "Harap cek dan periksa kembali file yang anda upload!"]);
                }
        }else{
            return redirect('/login')->with(['error' => "Harap login sebelum melakukan aksi!"]);
        }
    }
}
