<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\TrainingClass;
use Illuminate\Http\Request;
use App\Models\TrainingPlan;
use App\Models\TrainingOption;

class DikaltController extends Controller
{
    public function diklat()
    {
        $posts = TrainingPlan::select('id_training_plan', 'name_training_plan')->get();

            foreach($posts as $index) {
                $data[] = [
                    'id_training'       =>  $index->id_training_plan,
                    'training_name'     =>  $index->name_training_plan,
                ];
            }


        $respon =[
            'status'        => 'success',
            'message'       => 'Data Diklat',
            'data'          => [
                'status_code'   => 200,
                'data'          => $data,
            ],
        ];

        return response()->json($respon, 200);
    }

    public function kelas(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'training' => 'required',
        ]);

        $posts = TrainingClass::select('kd_class', 'name_class')->where('training_plan_id',$request->training)->get();

            foreach($posts as $index) {
                $data[] = [
                    'kd_class'       =>  $index->kd_class,
                    'name_class'     =>  $index->name_class,
                ];
            }


        $respon =[
            'status'        => 'success',
            'message'       => 'Data Kelas Diklat',
            'data'       => [
                'status_code'   => 200,
                'data'          => $data,
            ],
        ];

        return response()->json($respon, 200);
    }

    public function participant(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'training' => 'required',
            'class' => 'required',
        ]);

        $training = TrainingPlan::where('id_training_plan','=',$request->training)->first();
        $trainingclass = TrainingClass::where('kd_class','=',$request->class)->first();
        $post = TrainingOption::with(['takeTrainingPlan', 'takeClass', 'takeUserBiodata'])->where('training_plan_id', $request->training)->where('class_id', $trainingclass->id_class)->where('status','=','1')->get();
        $hitungpost = TrainingOption::where('training_plan_id', $request->training)->where('class_id', $trainingclass->id_class)->where('status','=','1')->count();

        if (!$post) {

            $respon =[
                'status'        => 'error',
                'message'       => 'Can not access your datas',
                'data'       => [
                    'status_code'   => 400,
                    'data'          => $post,
                ],
            ];

            return response()->json($respon, 400);
        }
        //sampai sini
        if ($hitungpost <= 0) {
            $participants = "No Participant entry";
        }else{

            foreach ($post as $index => $result) {
                $participants[] =[
                    'nip'       => $result->nip_user,
                    'name'      => $result->takeUserBiodata->name,
                    'email'     => $result->takeUserBiodata->takeUser->email,
                    'password'  => $result->takeUserBiodata->takeUser->password,
                    'gender'    => $result->takeUserBiodata->takeGender->gender,
                    'religion'  => $result->takeUserBiodata->takeReligion->religion,
                    'birth_day' => $result->takeUserBiodata->place_of_birth.", ".date('d-m-Y',strtotime($result->takeUserBiodata->birth_day)),
                    'instansi'  => $result->takeUserBiodata->takeAgency->agency,
                    'work_unit' => $result->takeUserBiodata->takeWorkUnit->work_unit,
                    'phone'     => $result->takeUserBiodata->phone,

                ];
            }
        }



        $respon =[
            'status'              => 'success',
            'training_id'         => $training->id_training_plan,
            'training_name'       => $training->name_training_plan,
            'training_start'      => $training->approved_start_date,
            'training_end'        => $training->approved_completion_date,
            'class'               => [
                'kd_class'        => $trainingclass->kd_class,
                'participant'     => $participants,
            ],
        ];

        return response()->json($respon, 200);
    }

    public function tambahan()
    {
        $post = TrainingOption::with(['takeTrainingPlan', 'takeClass', 'takeUserBiodata'])->where('status','=','1')->get();
        $hitungpost = TrainingOption::where('status','=','1')->count();

        if (!$post) {

            $respon =[
                'status'        => 'error',
                'message'       => 'Can not access your datas',
                'data'       => [
                    'status_code'   => 400,
                    'data'          => $post,
                ],
            ];

            return response()->json($respon, 400);
        }
        //sampai sini
        if ($hitungpost <= 0) {
            $participants = "No Participant entry";
        }else{

            foreach ($post as $index => $result) {
                $participants[] =[
                    'nip'           => $result->nip_user,
                    'name'          => $result->takeUserBiodata->name,
                    'email'         => $result->takeUserBiodata->takeUser->email,
                    'password'      => $result->takeUserBiodata->takeUser->password,
                    'gender'        => $result->takeUserBiodata->takeGender->gender,
                    'religion'      => $result->takeUserBiodata->takeReligion->religion,
                    'birth_day'     => $result->takeUserBiodata->place_of_birth.", ".date('d-m-Y',strtotime($result->takeUserBiodata->birth_day)),
                    'instansi'      => $result->takeUserBiodata->takeAgency->agency,
                    'work_unit'     => $result->takeUserBiodata->takeWorkUnit->work_unit,
                    'kd_class'      => $result->takeClass->kd_class,
                    'id_training'   => $result->training_plan_id,
                    'phone'         => $result->takeUserBiodata->phone,

                ];
            }
        }



        $respon =[
            'status'              => 'success',
            'message'             => 'Diklat Participant',
            'data'                => [
                    'status_code'   => 200,
                    'data'          => $participants,
            ],
        ];

        return response()->json($respon, 200);
    }

}
