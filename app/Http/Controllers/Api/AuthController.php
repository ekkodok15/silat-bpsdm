<?php

namespace App\Http\Controllers\Api;


use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Models\User;
use Exception;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validate->fails()) {
            $respon = [
                'status'    => 'error',
                'message'   => 'Validation Error!',
                'content'   => null,
            ];

            return response()->json($respon, 401);
        }else{
            $credentials    = request(['email','password']);
            $credentials    = Arr::add($credentials,'status','1');


        if (!Auth::attempt($credentials))
        {
            $respon =[
                'status'        => 'error',
                'message'       => 'Unathorized',
                'errors'        => null,
                'content'       => null,
            ];
            return response()->json($respon, 401);
        }

            $user = User::where('email', $request->email)->first();
            if (!Hash::check($request->password, $user->password, [])) {
                throw new Exception("Error in login");
            }

            $tokenResult = $user->createToken('we_are_team_h')->plainTextToken;

            $respon =[
                'status'        => 'success',
                'message'       => 'Log In Successfully',
                'errors'        => null,
                'content'       => [
                    'status_code'   => 200,
                    'access_token'  => $tokenResult,
                    'token_type'    => 'Bearer',
                ],
            ];
            return response()->json($respon, 200);
        }
    }

    // method for user logout and delete token
    public function logout(Request $request)
    {
        $user  =  $request->user();
        $user->currentAccessToken()->delete();

        $respon = [
                'status'        => 'error',
                'message'       => 'Logout Successfully',
                'errors'        => null,
                'content'       => null,
        ];
        return response()->json($respon, 200);
    }
}
