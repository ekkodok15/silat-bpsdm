<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
Use App\Models\TrainingPlan;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        if (!empty(Auth::User())) {
            if (Auth::User()->level_user_id == 1) {
                return redirect('/profile/pilih-asn');
            }else{
                return redirect()->route('home');
            }
        }else{
            $training = TrainingPlan::whereYear( 'regist_start_date',  date('Y') )->whereMonth('regist_start_date',  date('m'))->get();
            return view('starting.start-home', compact('training'));
        }

    }
}
