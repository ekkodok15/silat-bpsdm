<?php

namespace App\Http\Controllers\Admin;

Use App\Models\ImplementationPattern;
use App\Http\Controllers\Controller;
Use App\Models\TypeOfImplementation;
Use App\Models\TrainingClass;
use Illuminate\Http\Request;
Use App\Models\TrainingPlan;
Use App\Models\BudgetSource;
Use App\Models\Training;
Use Auth;

class PengajuandiklatController extends Controller
{

    public function index()
    {
        $trainingPlan = TrainingPlan::with(['takeAdmin','takeTraining','takeTypeOfImplementation','takeImplementationPattern','takeBudgetSource','takeStatus'])->whereIn('status', ['1','3'])->orderBy('status', 'ASC')->get();
        return view('admin.training.pengajuan.index',compact('trainingPlan'));
    }

    public function create()
    {
        $implementationPattern = ImplementationPattern::all();
        $typeOfImplementation = TypeOfImplementation::all();
        $budgetSource = BudgetSource::all();
        $training = Training::all();
        return view('admin.training.pengajuan.create',compact('implementationPattern','typeOfImplementation','budgetSource','training'));
    }

    public function store(Request $request)
    {
        $trainingplan                                     = new TrainingPlan;
        $trainingplan->admin_id                           = Auth::User()->id_admin;
        $trainingplan->name_training_plan                 = $request->name;
        $trainingplan->training_id                        = $request->training;
        $trainingplan->implementation_pattern_id          = $request->implementationPattern;
        $trainingplan->budget_source_id                   = $request->budgetSource;
        $trainingplan->type_of_implementation_id          = $request->typeOfImplementation;
        $trainingplan->training_location                  = $request->location;
        $trainingplan->proposed_start_date                = $request->tgl_mulai;
        $trainingplan->proposed_completion_date           = $request->tgl_selesai;
        $trainingplan->status                             = 1;
        $trainingplan->save();

        if ($trainingplan == true) {
            return redirect()->route('pengusulan.diklat');
        }else {
            return redirect()->route('pengusulan.diklat');
        }

    }

    public function show($id_training_plan)
    {
        $implementationPattern = ImplementationPattern::all();
        $typeOfImplementation = TypeOfImplementation::all();
        $budgetSource = BudgetSource::all();
        $training = Training::all();
        $trainingplan = TrainingPlan::with(['takeAdmin','takeTraining','takeTypeOfImplementation','takeImplementationPattern','takeBudgetSource','takeStatus'])->where('id_training_plan','=',$id_training_plan)->first();
        return view('admin.training.pengajuan.view', compact('trainingplan','implementationPattern','typeOfImplementation','budgetSource','training'));
    }

    public function edit($id_training_plan)
    {
        $implementationPattern = ImplementationPattern::all();
        $typeOfImplementation = TypeOfImplementation::all();
        $budgetSource = BudgetSource::all();
        $training = Training::all();
        $trainingplan = TrainingPlan::with(['takeAdmin','takeTraining','takeTypeOfImplementation','takeImplementationPattern','takeBudgetSource','takeStatus'])->where('id_training_plan','=',$id_training_plan)->first();
        $defaultSKU      = TrainingClass::max("id_class") + 1;
        $roman          = $this->numberToRomanRepresentation($defaultSKU);
        return view('admin.training.pengajuan.update', compact('trainingplan','implementationPattern','typeOfImplementation','budgetSource','training','defaultSKU','roman'));
    }

    public function update(Request $request, $id_training_plan)
    {
        $trainingplan = TrainingPlan::where('id_training_plan', $id_training_plan)
                        ->update([
                            'approved_start_date' => $request->setuju_tgl_mulai,
                            'approved_completion_date' => $request->setuju_tgl_selesai,
                            'regist_start_date' => $request->regist_tgl_mulai,
                            'regist_completion_date' => $request->regist_tgl_selesai,
                            'status' => 2
                        ]);
        if ($trainingplan == true) {

            $class                      = new TrainingClass;
            $class->name_class          = $request->name_class;
            $class->kd_class            = $request->kd_class;
            $class->training_plan_id    = $id_training_plan;
            $class->area                = $request->area;
            $class->address             = $request->address;
            $class->capacity            = 40;
            $class->Save();
            if ($class==true) {
                return redirect()->route('pengusulan.diklat');
            }else {
                Error;
            }
        }else {
            Error;
        }
    }

    public function destroy(Request $request)
    {
        $trainingplan = TrainingPlan::where('id_training_plan', $request->id_training_plan)
                                    ->update(['status' => '3']);

        if ($trainingplan == true) {
            return redirect()->route('pengusulan.diklat');
        }
    }


	function numberToRomanRepresentation($number) {
	    $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
	    $returnValue="";
	    while ($number > 0) {
	        foreach ($map as $roman => $int) {
	            if($number >= $int) {
	                $number -= $int;
	                $returnValue .= $roman;
	                break;
	            }
	        }
	    }
	    return $returnValue;
	}
}
