<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
Use App\Models\TypeOfTraining;
use Illuminate\Http\Request;

class KategorijenisdiklatController extends Controller
{

    public function index()
    {
        $typeoftraining = TypeOfTraining::all();
        return view('admin.training.kategori.index',compact('typeoftraining'));
    }

    public function create()
    {
        return view('admin.training.kategori.create');
    }

    public function store(Request $request)
    {
        $typeoftraining                     = new TypeOfTraining;
        $typeoftraining->type_of_training   = $request->category;
        $typeoftraining->save();

        if ($typeoftraining == true) {
            return redirect()->route('jenis.diklat');
        }else {
            return redirect()->route('jenis.diklat');
        }
    }

    public function show($id_type_of_training)
    {
        //
    }

    public function edit($id_type_of_training)
    {
        $typeoftraining = TypeOfTraining::where('id_type_of_training','=',$id_type_of_training)->first();
        return view('admin.training.kategori.update', compact('typeoftraining'));
    }

    public function update(Request $request, $id_type_of_training)
    {
        $typeoftraining = TypeOfTraining::where('id_type_of_training', $id_type_of_training)
                        ->update(['type_of_training' => $request->category]);
        if ($typeoftraining == true) {
            return redirect()->route('jenis.diklat');
        }else {
            Error;
        }
    }

    public function destroy(Request $request)
    {
        $typeoftraining = TypeOfTraining::where('id_type_of_training','=', $request->id_type_of_training)->delete();

        if ($typeoftraining == true) {
            return redirect()->route('jenis.diklat');
        }
    }
}
