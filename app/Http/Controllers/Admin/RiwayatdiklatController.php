<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ParticipantExport;
Use App\Models\ImplementationPattern;
use App\Http\Controllers\Controller;
Use App\Models\TypeOfImplementation;
use Illuminate\Http\Request;
Use App\Models\TrainingPlan;
Use App\Models\BudgetSource;
Use App\Models\Training;
use App\Models\TrainingOption;
Use Auth;
use ZipArchive;
use Maatwebsite\Excel\Facades\Excel as Excel;

class RiwayatdiklatController extends Controller
{
    public function diklatDisetujui()
    {
        $trainingPlan = TrainingPlan::with(['takeAdmin','takeTraining','takeTypeOfImplementation','takeImplementationPattern','takeBudgetSource','takeStatus'])->where('status', '=', '2')->orderBy('status', 'ASC')->get();
        return view('admin.history.disetujui.index',compact('trainingPlan'));
    }

    public function diklatBerjalan()
    {
        $trainingPlan = TrainingPlan::with(['takeAdmin','takeTraining','takeTypeOfImplementation','takeImplementationPattern','takeBudgetSource','takeStatus'])->where('status', '=', '4')->orderBy('status', 'ASC')->get();
        return view('admin.history.berjalan.index',compact('trainingPlan'));
    }

    public function diklatSelesai()
    {
        $trainingPlan = TrainingPlan::with(['takeAdmin','takeTraining','takeTypeOfImplementation','takeImplementationPattern','takeBudgetSource','takeStatus'])->where('status', '=', '5')->orderBy('status', 'ASC')->get();
        return view('admin.history.selesai.index',compact('trainingPlan'));
    }

    public function showDisetujui($id_training_plan)
    {
        $implementationPattern = ImplementationPattern::all();
        $typeOfImplementation = TypeOfImplementation::all();
        $budgetSource = BudgetSource::all();
        $training = Training::all();
        $trainingplan = TrainingPlan::with(['takeAdmin','takeTraining','takeTypeOfImplementation','takeImplementationPattern','takeBudgetSource','takeStatus'])->where('id_training_plan','=',$id_training_plan)->first();
        return view('admin.history.disetujui.view', compact('trainingplan','implementationPattern','typeOfImplementation','budgetSource','training'));
    }

    public function showDisetujuiParticipant($id_training_plan)
    {
        $id = $id_training_plan;
        $trainingparticipan = TrainingOption::with(['takeTrainingPlan', 'takeClass', 'takeUserBiodata'])->where('training_plan_id','=', $id_training_plan)->get();
        return view('admin.history.disetujui.participant', compact('trainingparticipan','id'));
    }

    public function setujuiParticipant($id_training_option)
    {
        $init = TrainingOption::where('id_training_option', $id_training_option)->first();
        $status = TrainingOption::where('id_training_option', $id_training_option)
                        ->update(['status' => "1", 'deskripsi' => "Selamat anda telah menjadi peserta diklat dan anda dapat memulai pembelajaran melalui lms dengan  akun yang anda miliki di aplikasi silat ini."]);
        if ($status == true) {
            return redirect()->route('diklat.disetujui.participant',$init->training_plan_id)->with(['success' => "Selamat anda berhasil melakukan approve pada peserta !"]);
        }else {
            return redirect()->route('diklat.disetujui.participant',$init->training_plan_id)->with(['error' => "terjadi kesalahan pada approve !"]);
        }
    }

    public function tolakParticipantView($id_training_option)
    {
        $trainingparticipan = TrainingOption::with(['takeTrainingPlan', 'takeClass', 'takeUserBiodata'])->where('id_training_option','=', $id_training_option)->first();
        return view('admin.history.disetujui.tolak', compact('trainingparticipan'));
    }

    public function tolakParticipant(Request $request)
    {
        $init = TrainingOption::where('id_training_option', $request->id)->first();
        $status = TrainingOption::where('id_training_option', $request->id)
                        ->update(['status' => "2", 'deskripsi' => $request->deskripsi]);
        if ($status == true) {
            return redirect()->route('diklat.disetujui.participant',$init->training_plan_id)->with(['success' => "Anda Menolak Peserta untuk mengikuti kegiata !"]);
        }else {
            return redirect()->route('diklat.disetujui.participant',$init->training_plan_id)->with(['error' => "Terjadi kesalahan pada penolakan !"]);
        }
    }

    public function editDisetujui($id_training_plan)
    {
        $implementationPattern = ImplementationPattern::all();
        $typeOfImplementation = TypeOfImplementation::all();
        $budgetSource = BudgetSource::all();
        $training = Training::all();
        $trainingplan = TrainingPlan::with(['takeAdmin','takeTraining','takeTypeOfImplementation','takeImplementationPattern','takeBudgetSource','takeStatus'])->where('id_training_plan','=',$id_training_plan)->first();
        return view('admin.history.disetujui.update', compact('trainingplan','implementationPattern','typeOfImplementation','budgetSource','training'));
    }

    public function updateDisetujui(Request $request, $id_training_plan)
    {
        $trainingplan = TrainingPlan::where('id_training_plan', $id_training_plan)
                        ->update([
                            'approved_start_date'       => $request->setuju_tgl_mulai,
                            'approved_completion_date'  => $request->setuju_tgl_selesai,
                            'regist_start_date'         => $request->regist_tgl_mulai,
                            'regist_completion_date'    => $request->regist_tgl_selesai
                        ]);
        if ($trainingplan == true) {
            return redirect()->route('diklat.disetujui');
        }else {
            Error;
        }
    }

    public function showBerjalan($id_training_plan)
    {
        $implementationPattern = ImplementationPattern::all();
        $typeOfImplementation = TypeOfImplementation::all();
        $budgetSource = BudgetSource::all();
        $training = Training::all();
        $trainingplan = TrainingPlan::with(['takeAdmin','takeTraining','takeTypeOfImplementation','takeImplementationPattern','takeBudgetSource','takeStatus'])->where('id_training_plan','=',$id_training_plan)->first();
        return view('admin.history.disetujui.view', compact('trainingplan','implementationPattern','typeOfImplementation','budgetSource','training'));
    }

    public function editBerjalan($id_training_plan)
    {
        $implementationPattern = ImplementationPattern::all();
        $typeOfImplementation = TypeOfImplementation::all();
        $budgetSource = BudgetSource::all();
        $training = Training::all();
        $trainingplan = TrainingPlan::with(['takeAdmin','takeTraining','takeTypeOfImplementation','takeImplementationPattern','takeBudgetSource','takeStatus'])->where('id_training_plan','=',$id_training_plan)->first();
        return view('admin.history.disetujui.update', compact('trainingplan','implementationPattern','typeOfImplementation','budgetSource','training'));
    }

    public function updateBerjalan(Request $request, $id_training_plan)
    {
        $trainingplan = TrainingPlan::where('id_training_plan', $id_training_plan)
                        ->update([
                            'approved_start_date'       => $request->setuju_tgl_mulai,
                            'approved_completion_date'  => $request->setuju_tgl_selesai,
                            'regist_start_date'         => $request->regist_tgl_mulai,
                            'regist_completion_date'    => $request->regist_tgl_selesai
                        ]);
        if ($trainingplan == true) {
            return redirect()->route('diklat.disetujui');
        }else {
            Error;
        }
    }

    public function showSelesai($id_training_plan)
    {
        $implementationPattern = ImplementationPattern::all();
        $typeOfImplementation = TypeOfImplementation::all();
        $budgetSource = BudgetSource::all();
        $training = Training::all();
        $trainingplan = TrainingPlan::with(['takeAdmin','takeTraining','takeTypeOfImplementation','takeImplementationPattern','takeBudgetSource','takeStatus'])->where('id_training_plan','=',$id_training_plan)->first();
        return view('admin.history.disetujui.view', compact('trainingplan','implementationPattern','typeOfImplementation','budgetSource','training'));
    }

    public function dataCsv($id)
    {
        $trainingplant  = TrainingPlan::where('id_training_plan',$id)->first();
        $name           = $trainingplant->name_training_plan.'.csv';

        return Excel::download(new ParticipantExport($id), $name);
    }

    public function photoZip($id)
    {
        $data = TrainingOption::with(['takeUserBiodata'])->where('training_plan_id','=', $id)->where('status','1')->get();
        $trainingplant  = TrainingPlan::where('id_training_plan',$id)->first();
        $name           = "PAS-PHOTO-PESERTA-".$trainingplant->name_training_plan.'-'.time().'.zip';
        $tempat_zip = public_path() . "/public/images/";

        $zip = new ZipArchive;
        if ($zip->open( $tempat_zip . $name, ZipArchive::CREATE) === TRUE) {
            foreach($data as $d)
            {
                $imagesName = public_path()."/public/images/".$d->takeUserBiodata->photo;
                // Add File in ZipArchive
                $zip->addFile($imagesName, $d->takeUserBiodata->photo);
            }
            // Close ZipArchive
            $zip->close();
        }

        // Set Header
        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );

        $filetopath=$tempat_zip. $name;
        // Create Download Response
        if(file_exists($filetopath)){
            return response()->download($filetopath,$name,$headers);
        }

        return redirect()->route('diklat.disetujui.participant');
    }

}
