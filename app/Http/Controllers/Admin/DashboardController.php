<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
Use App\Models\TrainingPlan;
use Illuminate\Http\Request;
Use App\Models\Admin;
Use App\Models\User;

class DashboardController extends Controller
{
    public function index()
    {
        $a = User::where('email_verified_at','!=',null)->count();
        $b = Admin::where('email_verified_at','!=',null)->where('level_admin_id', '=','3')->count();
        $c = TrainingPlan::whereYear( 'approved_start_date',  date('Y') )->count();
        $d = TrainingPlan::where('status', '=', '5' )->count();
        return view('admin.dashboard', compact('a','b','c','d'));
    }
}
