<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
Use App\Models\LevelAdmins;
Use App\Models\WorkUnit;
Use App\Models\Agency;
Use App\Models\Admin;

class AdminController extends Controller
{
    public function index()
    {
        $admin = Admin::with(['workUnitAdmin'])->where('level_admin_id', '!=', '1')->get();
        return view('admin.manajemen.admin.index', compact('admin'));
    }

    public function create()
    {
        $instansi = Agency::all();
        $unitKerja = WorkUnit::all();
        $levelAdmin = LevelAdmins::where('id_level_admin','!=','1')->get();
        return view('admin.manajemen.admin.create', compact('instansi','unitKerja','levelAdmin'));
    }

    public function updateStatus(Request $request)
    {
        $status = Admin::where('id_admin', $request->id)
                        ->update(['status' => $request->status]);
        if ($status == true) {
            return redirect('manajemen.admin');
        }else {
            Error;
        }
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $admin                      = new Admin;
        $admin->name                = $request->name;
        $admin->email               = $request->email;
        $admin->username            = $request->username;
        $admin->agency_id           = $request->instansi;
        $admin->work_unit_id        = $request->unitKerja;
        $admin->level_admin_id      = $request->levelAdmin;
        $admin->remember_token      = Str::random(60);
        $admin->status              = 0;
        $admin->save();

        if ($admin == true) {
            return redirect()->route('manajemen.admin');
        }else {
            return redirect()->route('manajemen.admin');
        }

    }

    public function show($id_admin)
    {
        //
    }

    public function edit($id_admin)
    {
        $instansi = Agency::all();
        $unitKerja = WorkUnit::all();
        $levelAdmin = LevelAdmins::where('id_level_admin','!=','1')->get();
        $admin = Admin::where('id_admin','=',$id_admin)->first();
        return view('admin.manajemen.admin.update', compact('admin','instansi','unitKerja','levelAdmin'));
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy(Request $request)
    {
        $admin = Admin::where('id_admin','=', $request->id_admin)->delete();

        if ($admin == true) {
            return redirect('/administrator');
        }
    }
}
