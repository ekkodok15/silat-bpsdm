<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
Use App\Models\ParticipantType;
use Illuminate\Http\Request;
Use App\Models\UserBiodata;
use Illuminate\Support\Str;
Use App\Models\LevelUser;
Use App\Models\WorkUnit;
Use App\Models\Religion;
Use App\Models\Agency;
Use App\Models\Gender;
Use App\Models\User;
Use App\Models\Rank;

class UserController extends Controller
{
    public function index()
    {
        $user = UserBiodata::with(['takeUser','takeGender','takeReligion','takeParticipantType','takeGroup','takeRank','takeAgency','takeWorkUnit'])->get();
        return view('admin.manajemen.peserta.index', compact('user'));
    }

    public function reserPw($nip_user)
    {
        $status = User::where('nip_user', $nip_user)
                        ->update(['password' => bcrypt($nip_user)]);
        if ($status == true) {
            return redirect()->route('manajemen.user');
        }else {
            echo "Error";
        }
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
