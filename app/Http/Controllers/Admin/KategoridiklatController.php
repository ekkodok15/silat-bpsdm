<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
Use App\Models\TypeOfTraining;
use Illuminate\Http\Request;
Use App\Models\Training;

class KategoridiklatController extends Controller
{
    public function index()
    {
        $training = Training::with(['typeTraining'])->get();
        return view('admin.training.training.index',compact('training'));
    }

    public function create()
    {
        $typeoftraining = TypeOfTraining::all();
        return view('admin.training.training.create',compact('typeoftraining'));
    }

    public function store(Request $request)
    {
        $training                       = new Training;
        $training->training             = $request->training;
        $training->type_of_training_id  = $request->category;
        $training->save();

        if ($training == true) {
            return redirect()->route('diklat');
        }else {
            return redirect()->route('diklat');
        }
    }

    public function show($id_training)
    {
        //
    }

    public function edit($id_training)
    {
        $typeoftraining = TypeOfTraining::all();
        $training = Training::with(['typeTraining'])->where('id_training','=',$id_training)->first();
        return view('admin.training.training.update',compact('training','typeoftraining'));
    }

    public function update(Request $request, $id_training)
    {
        $training = Training::where('id_training', $id_training)
                                        ->update(['training' => $request->training,'type_of_training_id'=> $request->category]);
        if ($training == true) {
            return redirect()->route('diklat');
        }else {
            Error;
        }
    }

    public function destroy(Request $request)
    {
        $training = Training::where('id_training','=', $request->id_training)->delete();

        if ($training == true) {
            return redirect()->route('diklat');
        }
    }
}
