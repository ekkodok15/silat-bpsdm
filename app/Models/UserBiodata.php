<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBiodata extends Model
{
    use HasFactory;

    protected $tabel = "user_biodata";
    protected $primaryKey = "nip";
    public $keyType = 'string';

	protected $fillable = [
        'nip',
        'name',
        'gender_id',
        'religion_id',
        'place_of_birth',
        'birth_day',
        'phone',
        'participant_type_id',
        'group_id',
        'rank_id',
        'agency_id',
        'work_unit_id',
        'photo',
    ];

    function takeUser(){
		return $this->hasOne('App\Models\User','nip_user','nip');
	}

    function takeGender(){
		return $this->belongsTo('App\Models\Gender','gender_id', 'id_gender');
	}

    function takeReligion(){
		return $this->belongsTo('App\Models\Religion','religion_id', 'id_religion');
	}

    function takeParticipantType(){
		return $this->belongsTo('App\Models\ParticipantType','participant_type_id', 'id_participant_type');
	}

    function takeGroup(){
		return $this->belongsTo('App\Models\Group','group_id', 'id_group');
	}

    function takeRank(){
		return $this->belongsTo('App\Models\Rank','rank_id', 'id_rank');
	}

    function takeAgency(){
		return $this->belongsTo('App\Models\Agency','agency_id', 'id_agency');
	}

    function takeWorkUnit(){
		return $this->belongsTo('App\Models\WorkUnit','work_unit_id', 'id_work_unit');
	}

}
