<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkUnit extends Model
{
    use HasFactory;

    function workUnitAdmin(){
    	return $this->hasMany('App\Models\Admin', 'work_unit_id');
    }
}
