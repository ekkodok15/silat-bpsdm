<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    use HasFactory;

    protected $tabel = "trainings";
    protected $primaryKey = "id_training";

    protected $fillable = [
        'training',
        'type_of_training_id',
    ];

    public function typeTraining(){
    	return $this->belongsTo('App\Models\TypeOfTraining','type_of_training_id', 'id_type_of_training');
        // return $this->belongsTo(WorkUnit::class);
    }
}
