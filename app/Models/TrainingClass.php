<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrainingClass extends Model
{
    use HasFactory;

    protected $tabel = "training_classes";
    protected $primaryKey = "id_class";
    protected $fillable = [
        'name_class',
        'training_plan_id',
        'kd_class',
        'area',
        'address',
        'capacity',
    ];
}
