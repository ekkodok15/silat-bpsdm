<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrainingOption extends Model
{
    use HasFactory;

    protected $tabel = "training_options";
    protected $primaryKey = "id_training_option";

    function takeTrainingPlan(){
        return $this->belongsTo('App\Models\TrainingPlan','training_plan_id','id_training_plan');
    }

    function takeClass(){
        return $this->belongsTo('App\Models\TrainingClass','class_id','id_class');
    }

    function takeUserBiodata(){
        return $this->belongsTo('App\Models\UserBiodata','nip_user','nip');
    }
}
