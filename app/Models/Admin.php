<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;

class Admin extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $tabel = "admins";
    protected $primaryKey = "id_admin";
    protected $fillable = [
        'username',
        'name',
        'email',
        'password',
        'level_admin_id',
        'agency_id',
        'work_unit_id',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function workUnitAdmin(){
    	return $this->belongsTo('App\Models\WorkUnit','work_unit_id', 'id_work_unit');
        // return $this->belongsTo(WorkUnit::class);
    }
}
