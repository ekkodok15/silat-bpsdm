<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrainingPlan extends Model
{
    use HasFactory;

    protected $tabel = "training_plans";
    protected $primaryKey = "id_training_plan";

    protected $fillable = [
        'admin_id',
        'name_training_plan',
        'training_id',
        'type_of_implementation_id',
        'proposed_start_date',
        'proposed_completion_date',
        'status',
        'approved_start_date',
        'approved_completion_date',
        'regist_start_date',
        'regist_completion_date',
        'implementation_pattern_id',
        'budget_source_id',
        'training_location',
    ];

    function takeAdmin(){
        return $this->belongsTo('App\Models\Admin','admin_id','id_admin');
    }

    public function takeTraining(){
    	return $this->belongsTo('App\Models\Training','training_id', 'id_training');
    }

    function takeTypeOfImplementation(){
		return $this->belongsTo('App\Models\TypeOfImplementation','type_of_implementation_id', 'id_type_of_implementation');
	}

    function takeImplementationPattern(){
		return $this->belongsTo('App\Models\ImplementationPattern','implementation_pattern_id', 'id_implementation_pattern');
	}

    function takeBudgetSource(){
		return $this->belongsTo('App\Models\BudgetSource','budget_source_id', 'id_budget_source');
	}

    function takeStatus(){
		return $this->belongsTo('App\Models\StatusTraining','status', 'id_status_training');
	}

}
