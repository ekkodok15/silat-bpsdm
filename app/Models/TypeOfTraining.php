<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeOfTraining extends Model
{
    use HasFactory;

    protected $tabel = "type_of_trainings";
    protected $primaryKey = "id_type_of_training";

    function trainingdapat(){
    	return $this->hasMany('App\Models\Training', 'type_of_training_id');
    }
}
