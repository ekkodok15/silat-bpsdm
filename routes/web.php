<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/beranda');
});

/*
|--------------------------------------------------------------------------
| Starter
|--------------------------------------------------------------------------
*/
Route::get('/beranda', "HomeController@index")->name('beranda');

/*
|--------------------------------------------------------------------------
| Login & Regist User
|--------------------------------------------------------------------------
*/
Route::get('/login', "Auth\AuthController@index")->name('login');
Route::get('/regist', "Auth\AuthController@regist")->name('regist');

/*
|--------------------------------------------------------------------------
| Login Admin
|--------------------------------------------------------------------------
*/
Route::get('/login/admin', "Auth\AuthController@loginAdmin")->name('login.admin');

/*
|--------------------------------------------------------------------------
| Proses Login & Regist
|--------------------------------------------------------------------------
*/
Route::post('login-proses', "Auth\AuthController@loginProses")->name('loginProses');
Route::post('regist-proses', "Auth\AuthController@registProses")->name('registProses');

/*
|--------------------------------------------------------------------------
| Logout All
|--------------------------------------------------------------------------
*/
Route::get('/logout', "Auth\AuthController@logout")->name('logout');

/*
|--------------------------------------------------------------------------
| User
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['auth:user']], function(){

    /*
    |--------------------------------------------------------------------------
    | User (Home)
    |--------------------------------------------------------------------------
    */
    Route::get('/home', "User\HomeController@index")->name('home');
    /*
    |--------------------------------------------------------------------------
    | User (Profile)
    |--------------------------------------------------------------------------
    */
    Route::get('/profile', "User\HomeController@profile")->name('profile');
    Route::get('/profile/pilih-asn', "User\HomeController@pilihAsn")->name('pilih.asn');
    Route::get('/profile/upload-foto', "User\HomeController@uploadPhoto")->name('upload.photo');
    Route::post('profile-update-asn', "User\HomeController@updateAsn")->name('updateAsn');
    Route::post('profile-update', "User\HomeController@updateProfile")->name('updateProfile');
    Route::post('profile-update-photo', "User\HomeController@updatePhoto")->name('updatePhoto');

    /*
    |--------------------------------------------------------------------------
    | User (Diklat)
    |--------------------------------------------------------------------------
    */
    Route::get('/diklat/choose/{id_taining_plan}', "User\DiklatController@choose")->name('diklat.user.pilih');
    Route::get('/diklat/all', "User\DiklatController@index")->name('diklat.all');
    Route::get('/diklat/manajerial', "User\DiklatController@manajerial")->name('diklat.manajerial');
    Route::get('/diklat/fungsional', "User\DiklatController@fungsional")->name('diklat.fungsional');
    Route::get('/diklat/teknis', "User\DiklatController@teknis")->name('diklat.teknis');
    Route::get('/diklat/history', "User\DiklatController@history")->name('diklat.user.hostory');
    Route::post('/diklat/choose/proses', "User\DiklatController@proses")->name('chooseDiklat');


});

Route::group(['middleware' => ['auth:admin']], function(){

    /*
    |--------------------------------------------------------------------------
    | Admin (Dashboard)
    |--------------------------------------------------------------------------
    */
    Route::get('/dashboard', "Admin\DashboardController@index")->name('dashboard');

    /*
    |--------------------------------------------------------------------------
    | Admin (Manajemen Admin) kerja
    |--------------------------------------------------------------------------
    */
    Route::get('/administrator', "Admin\AdminController@index")->name('manajemen.admin');
    Route::get('/administrator/add', "Admin\AdminController@create")->name('manajemen.admin.add');
    Route::get('/administrator/update/{id_admin}', "Admin\AdminController@edit")->name('manajemen.admin.update');
    Route::post('administrator-delete', "Admin\AdminController@destroy")->name('delete.admin');
    Route::post('administrator-update-status', "Admin\AdminController@updateStatus")->name('updateStatusAdmin');
    Route::post('administrator-create', "Admin\AdminController@store")->name('createNewAdmin');
    Route::post('administrator-update/{id_admin}', "Admin\AdminController@update")->name('updateAdmin');

    /*
    |--------------------------------------------------------------------------
    | Admin (Manajemen User)
    |--------------------------------------------------------------------------
    */
    Route::get('/user', "Admin\UserController@index")->name('manajemen.user');
    Route::get('/user/add', "Admin\UserController@newUser")->name('manajemen.user.add');
    Route::get('/user/update/{id_User}', "Admin\UserController@perbaruiUser")->name('manajemen.user.update');
    Route::post('user-delete', "Admin\UserController@destroy")->name('delete.user');
    Route::post('user-update-status', "Admin\UserController@updateStatus")->name('updateStatusUser');
    Route::post('user-create', "Admin\UserController@store")->name('createNewUser');
    Route::post('user-update/{id_User}', "Admin\UserController@update")->name('updateUser');
    Route::get('reset-pass/{nip_user}', "Admin\UserController@reserPw")->name('resetPassword');

    /*
    |--------------------------------------------------------------------------
    | Admin (Kategori Jenis DIklat)
    |--------------------------------------------------------------------------
    */
    Route::get('/training/type', "Admin\KategorijenisdiklatController@index")->name('jenis.diklat');
    Route::get('/training/type/add', "Admin\KategorijenisdiklatController@create")->name('jenis.diklat.add');
    Route::get('/training/type/update/{id_type_of_training}', "Admin\KategorijenisdiklatController@edit")->name('jenis.diklat.update');
    Route::post('training/tpye-delete', "Admin\KategorijenisdiklatController@destroy")->name('deleteCategory');
    Route::post('training/tpye-create', "Admin\KategorijenisdiklatController@store")->name('createNewCategory');
    Route::post('training/tpye-update/{id_type_of_training}', "Admin\KategorijenisdiklatController@update")->name('updateCategory');

    /*
    |--------------------------------------------------------------------------
    | Admin (Kategori Diklat)
    |--------------------------------------------------------------------------
    */
    Route::get('/training', "Admin\KategoridiklatController@index")->name('diklat');
    Route::get('/training/add', "Admin\KategoridiklatController@create")->name('diklat.add');
    Route::get('/training/update/{id_training}', "Admin\KategoridiklatController@edit")->name('diklat.update.masasih');
    Route::post('training/delete', "Admin\KategoridiklatController@destroy")->name('deleteTraining');
    Route::post('training/create', "Admin\KategoridiklatController@store")->name('createNewTraining');
    Route::post('training/update-proses/{id_training}', "Admin\KategoridiklatController@update")->name('updateTraining');

    /*
    |--------------------------------------------------------------------------
    | Admin (Pengusulan Diklat)
    |--------------------------------------------------------------------------
    */
    Route::get('/training/plan', "Admin\PengajuandiklatController@index")->name('pengusulan.diklat');
    Route::get('/training/plan/add', "Admin\PengajuandiklatController@create")->name('pengusulan.diklat.add');
    Route::get('/training/plan/view/{id_training_plan}', "Admin\PengajuandiklatController@show")->name('pengusulan.diklat.view');
    Route::get('/training/plan/update/{id_training_plan}', "Admin\PengajuandiklatController@edit")->name('pengusulan.diklat.update');
    Route::post('training/plan/delete', "Admin\PengajuandiklatController@destroy")->name('deleteTrainingPlan');
    Route::post('training/plan/create', "Admin\PengajuandiklatController@store")->name('createNewTrainingPlan');
    Route::post('training/plan/update/{id_training_plan}', "Admin\PengajuandiklatController@update")->name('updateTrainingPlan');

    /*
    |--------------------------------------------------------------------------
    | Admin (Diklat Disetujui)
    |--------------------------------------------------------------------------
    */
    Route::get('/training/approve', "Admin\RiwayatdiklatController@diklatDisetujui")->name('diklat.disetujui');
    Route::get('/training/approve/view/{id_training_plan}', "Admin\RiwayatdiklatController@showDisetujui")->name('diklat.disetujui.view');
    Route::get('/training/approve/participant/{id_training_plan}', "Admin\RiwayatdiklatController@showDisetujuiParticipant")->name('diklat.disetujui.participant');
    Route::get('/training/approve/participant/approve/{id_training_option}', "Admin\RiwayatdiklatController@setujuiParticipant")->name('diklat.disetujui.participant.approve');
    Route::get('/training/approve/participant/disapprove/{id_training_option}', "Admin\RiwayatdiklatController@tolakParticipantView")->name('diklat.disetujui.participant.disapprove.view');
    Route::get('/training/approve/update/{id_training_plan}', "Admin\RiwayatdiklatController@editDisetujui")->name('diklat.disetujui.update');
    Route::get('/training/approve/participant/photo/zip/{id}', "Admin\RiwayatdiklatController@photoZip")->name('photoZip');
    Route::get('/training/approve/participant/data/csv/{id}', "Admin\RiwayatdiklatController@dataCsv")->name('dataCsv');
    Route::post('training/approve/participant/disapprove/proses', "Admin\RiwayatdiklatController@tolakParticipant")->name('participantDisapproveProses');
    Route::post('training/approve/update/{id_training_plan}', "Admin\RiwayatdiklatController@updateDisetujui")->name('updateTrainingApproved');

    /*
    |--------------------------------------------------------------------------
    | Admin (Diklat Berjalan)
    |--------------------------------------------------------------------------
    */
    Route::get('/training/on-going', "Admin\RiwayatdiklatController@diklatBerjalan")->name('dikalt.berjalan');
    Route::get('/training/on-going/view/{id_training_plan}', "Admin\RiwayatdiklatController@showBerjalan")->name('diklat.berjalan.view');
    Route::get('/training/on-going/update/{id_training_plan}', "Admin\RiwayatdiklatController@editBerjalan")->name('diklat.berjalan.update');
    Route::post('training/on-going/update/{id_training_plan}', "Admin\RiwayatdiklatController@updateBerjalan")->name('updateTrainingOnGoing');

    /*
    |--------------------------------------------------------------------------
    | Admin (Diklat Selesai)
    |--------------------------------------------------------------------------
    */
    Route::get('/training/done', "Admin\RiwayatdiklatController@diklatSelesai")->name('dikalt.selesai');
    Route::get('/training/done/view/{id_training_plan}', "Admin\RiwayatdiklatController@showSelesai")->name('diklat.selesai.view');

    /*
    |--------------------------------------------------------------------------
    | Admin (Class)
    |--------------------------------------------------------------------------
    */
    Route::get('/training/class/{id_training_plan}', "Admin\ClasstrainingController@show")->name('class.diklat');

});
