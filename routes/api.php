<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\DikaltController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//API route for register new user
Route::post('register', [AuthController::class, 'register']);
//API route for login user
Route::post('login', [AuthController::class, 'login']);

//Protecting Routes
Route::group(['prefix' => 'v1', 'middleware' => 'auth:sanctum'], function () {
    // API route for Get Diklat Datas
    Route::get('getDiklat', [DikaltController::class, 'diklat']);

    // API route for Get Diklat Datas
    Route::post('getClass', [DikaltController::class, 'kelas']);

    // API route for Get Diklat Datas
    Route::post('getDiklatParticipant', [DikaltController::class, 'participant']);

    // API route for Get Diklat Datas
    Route::get('getParticipant', [DikaltController::class, 'tambahan']);

    // API route for logout user
    Route::post('logout', [AuthController::class, 'logout']);
});
