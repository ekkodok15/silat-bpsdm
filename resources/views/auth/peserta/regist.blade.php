@extends('layout.auth.peserta.master')
@section('content')
    <div class="card login-card">
        <div class="card-body">
            <h3 class="text-center text-white font-weight-light mb-4">REGISTRASI</h3>
            <form action="{{route('registProses')}}" method="post">
			    @csrf
                <div class="form-group">
                    <input type="text" id="nip" name="nip" placeholder="NIP/NRP/NPP/NIK" class="form-control">
                </div>
                <div class="form-group">
                    <input type="text" id="name" name="name" placeholder="Nama Lengkap" class="form-control">
                </div>
                <div class="form-group">
                    <input type="email" id="email" name="email" placeholder="Email" class="form-control">
                </div>
                <div class="form-group">
                    <input type="password" id="password" name="password" placeholder="Password" class="form-control">
                </div>
                <button type="submit" class="btn btn-danger btn-block mb-3">Registrasi</button>
            </form>
            <div class="d-flex justify-content-between mt-4">
                <p class="text-white text-center font-weight-light">Register with</p>
                <p class="text-center mb-0">
                    <a href="#" class="social-login-btn icon-gmail"><i class="mdi mdi-gmail"></i></a>
                </p>
            </div>
                <p class="text-white text-left font-weight-light">Have account?<a href="{{route('login')}}"> Login Now !!!</p>
        </div>
    </div>
@endsection
