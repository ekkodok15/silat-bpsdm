@extends('layout.auth.peserta.master')
@section('content')
    <div class="card login-card">
        <div class="card-body">
            @if(!empty(session()->get( 'error' )))
                    <div class="alert alert-danger" role="alert">
                    {{ session()->get( 'error' ) }}
                    </div>
            @endif
            @if(!empty(session()->get( 'success' )))
                    <div class="alert alert-success" role="alert">
                        {{ session()->get( 'success' ) }}
                    </div>
            @endif
            <h3 class="text-center text-white font-weight-light mb-4">LOG IN</h3>
            <form action="{{route('loginProses')}}" method="post">
			    @csrf
                <div class="form-group">
                    <input type="email" id="email" name="email" placeholder="Email" class="form-control">
                </div>
                <div class="form-group">
                    <input type="password" id="password" name="password" placeholder="Password" class="form-control">
                </div>
                <button type="submit" class="btn btn-danger btn-block mb-3">Login</button>
            </form>
            <div class="d-flex justify-content-between mt-4">
                <p class="text-white text-center font-weight-light">Login with</p>
                <p class="text-center mb-0">
                    <a href="#" class="social-login-btn icon-gmail"><i class="mdi mdi-gmail"></i></a>
                </p>
            </div>
                <p class="text-white text-left font-weight-light">Don't have account?<a href="{{route('regist')}}"> Create Now !!!</p>
        </div>
    </div>
@endsection
