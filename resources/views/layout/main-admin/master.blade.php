<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Skydash Admin</title>

  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ asset('assets/admin/template/vendors/feather/feather.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/admin/template/vendors/ti-icons/css/themify-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/admin/template/vendors/css/vendor.bundle.base.css') }}">
  <!-- endinject -->

  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="{{ asset('assets/admin/template/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/admin/template/vendors/ti-icons/css/themify-icons.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/template/js/select.dataTables.min.css') }}">
  <!-- End plugin css for this page -->

  <!-- inject:css -->
  <link rel="stylesheet" href="{{ asset('assets/admin/template/css/vertical-layout-light/style.css') }}">
  <!-- endinject -->

  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="{{ asset('assets/admin/template/vendors/dropify/dist/css/dropify.min.css') }}">
  <!-- End plugin css for this page -->

  <link rel="shortcut icon" href="{{ asset('assets/admin/template/images/favicon.png') }}" />
</head>
<body>
  <div class="container-scroller">

    @include('layout.main-admin.navbar')

    <div class="container-fluid page-body-wrapper">

        @include('layout.main-admin.seting-panel')

        @include('layout.main-admin.sidebar')

        <div class="main-panel">

        @yield('content')

        @include('layout.main-admin.footer')

        </div>
      <!-- main-panel ends -->

    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="{{ asset('assets/admin/template/vendors/js/vendor.bundle.base.js') }}"></script>
  <!-- endinject -->

  <!-- Plugin js for this page -->
  <script src="{{ asset('assets/admin/template/vendors/chart.js/Chart.min.js') }}"></script>
  <script src="{{ asset('assets/admin/template/vendors/datatables.net/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('assets/admin/template/vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
  <script src="{{ asset('assets/admin/template/js/dataTables.select.min.js') }}"></script>
  <!-- End plugin js for this page -->

  <!-- inject:js -->
  <script src="{{ asset('assets/admin/template/js/off-canvas.js') }}"></script>
  <script src="{{ asset('assets/admin/template/js/hoverable-collapse.js') }}"></script>
  <script src="{{ asset('assets/admin/template/js/template.js') }}"></script>
  <script src="{{ asset('assets/admin/template/js/settings.js') }}"></script>
  <script src="{{ asset('assets/admin/template/js/todolist.js') }}"></script>
  <!-- endinject -->

  @yield('js')

  <!-- Custom js for this page-->
  <script src="{{ asset('assets/admin/template/js/dashboard.js') }}"></script>
  <script src="{{ asset('assets/admin/template/js/Chart.roundedBarCharts.js') }}"></script>
  <!-- End custom js for this page-->

  <!-- Custom js for this page-->
  <script src="{{ asset('assets/admin/template/js/data-table.js') }}"></script>
  <!-- End custom js for this page-->

  <!-- Plugin js for this page-->
  <script src="{{ asset('assets/admin/template/vendors/sweetalert/sweetalert.min.js') }}"></script>
  <script src="{{ asset('assets/admin/template/vendors/jquery.avgrund/jquery.avgrund.min.js') }}"></script>
  <!-- End plugin js for this page-->

  <!-- Custom js for this page-->
  <script src="{{ asset('assets/admin/template/js/alerts.js') }}"></script>
  <script src="{{ asset('assets/admin/template/js/avgrund.js') }}"></script>
  <!-- End custom js for this page-->

  <!-- Custom js for this page-->
  <script src="{{ asset('assets/admin/template/js/modal-demo.js') }}"></script>
  <!-- End custom js for this page-->

  <!-- Plugin js for this page -->
  <script src="{{ asset('assets/admin/template/vendors/dropify/dist/js/dropify.min.js') }}"></script>
  <!-- End plugin js for this page -->

  <!-- Custom js for this page-->
  <script src="{{ asset('assets/admin/template/js/dropify.js') }}"></script>
  <!-- End custom js for this page-->
</body>

</html>
