<!-- partial:partials/_footer.html -->
<footer class="footer">
  <div class="d-sm-flex justify-content-center justify-content-sm-between">
    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © {{ date("Y") }}.  Dropship System Created By <a href="https://digihelp.id/">DigiHelp.id.</a></span>
  </div>
</footer>
<!-- partial -->