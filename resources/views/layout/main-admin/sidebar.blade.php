<!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="{{ route('dashboard') }}">
              <i class="icon-grid menu-icon"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#user" aria-expanded="false" aria-controls="user">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">User</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="user">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="{{ route('manajement.admin') }}">Admin</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{ route('manajement.costumer') }}">Costumer</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('manajement.slider') }}">
              <i class="icon-columns menu-icon"></i>
              <span class="menu-title">Slider</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#product" aria-expanded="false" aria-controls="product">
              <i class="icon-book menu-icon"></i>
              <span class="menu-title">Product</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="product">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="{{ route('manajement.category.product') }}">Category Product</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{ route('manajement.product') }}">Product</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('manajement.insight') }}">
              <i class="icon-align-left menu-icon"></i>
              <span class="menu-title">Insight</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('manajement.course') }}">
              <i class="icon-open menu-icon"></i>
              <span class="menu-title">Course</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#order" aria-expanded="false" aria-controls="order">
                <i class="icon-tag menu-icon"></i>
                <span class="menu-title">Order</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="order">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="{{ route('manajement.order') }}">All Orders</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{ route('manajement.order.new') }}">New Order</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{ route('manajement.order.prepared') }}">Prepared</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{ route('manajement.order.ready.to.ship') }}">Ready To Ship</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{ route('manajement.order.in.delivery') }}">In Delivery</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{ route('manajement.order.complained') }}">Complained</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{ route('manajement.order.completed') }}">Order Completed</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{ route('manajement.order.canceled') }}">Order Canceled</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('manajement.report') }}">
                <i class="icon-paper menu-icon"></i>
                <span class="menu-title">Report</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('manajement.wd') }}">
                <i class="icon-esc menu-icon"></i>
                <span class="menu-title">Withdrawal Request</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#general-setting" aria-expanded="false" aria-controls="general-setting">
              <i class="icon-loader menu-icon"></i>
              <span class="menu-title">General Setting</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="general-setting">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="{{ route('manajement.sk') }}">T&C</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{ route('manajement.faq') }}">FAQ</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{ route('manajement.contact.us') }}">Contact Us</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#setting" aria-expanded="false" aria-controls="setting">
              <i class="icon-cog menu-icon"></i>
              <span class="menu-title">Setting</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="setting">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="{{ route('manajement.app') }}">lainnya</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </nav>
      <!-- partial -->
