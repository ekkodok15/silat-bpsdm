<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/peserta2/docs/assets/images/logo.png')}}">
  <link rel="icon" type="image/x-icon" href="{{ asset('assets/peserta2/docs/assets/images/logo.png')}}">
  <script language='JavaScript'>
        var txt=" Administrator - SiLat || BPSDM PROVISI GORONTALO -";
        var speed=300;
        var refresh=null;
        function action() { document.title=txt;
        txt=txt.substring(1,txt.length)+txt.charAt(0);
        refresh=setTimeout("action()",speed);}action();
	</script>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
  <!-- Nucleo Icons -->
  <link href="{{ asset('assets/admin2/assets/css/nucleo-icons.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/admin2/assets/css/nucleo-svg.css') }}" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="{{ asset('assets/admin2/assets/css/material-dashboard.css?v=3.0.0') }}" rel="stylesheet" />
</head>

<body class="bg-gray-200">
    @include('layout.auth.admin.header')
<main class="main-content  mt-0">
    <div class="page-header align-items-start min-vh-100" style="background-image: url('{{ asset('assets/admin2/assets/img/bagian-depan.png')}}');">
      <span class="mask bg-gradient-dark opacity-6"></span>
      <div class="container my-auto">
    @yield('content')
        </div>
    @include('layout.auth.admin.footer')
      </div>
  </main>
    
  <!--   Core JS Files   -->
  <script src="{{ asset('assets/admin2/assets/js/core/popper.min.js') }}"></script>
  <script src="{{ asset('assets/admin2/assets/js/core/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/admin2/assets/js/plugins/perfect-scrollbar.min.js') }}"></script>
  <script src="{{ asset('assets/admin2/assets/js/plugins/smooth-scrollbar.min.js') }}"></script>
  <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
      var options = {
        damping: '0.5'
      }
      Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
  </script>
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('assets/admin2/assets/js/material-dashboard.min.js?v=3.0.0') }}"></script>
</body>

</html>
