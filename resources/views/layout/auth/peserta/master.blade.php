<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="{{ asset('assets/peserta2/docs/assets/images/logo.png')}}" type="image/x-icon">
    <script language='JavaScript'>
        var txt="- SiLat || BPSDM PROVISI GORONTALO -";
        var speed=300;
        var refresh=null;
        function action() { document.title=txt;
        txt=txt.substring(1,txt.length)+txt.charAt(0);
        refresh=setTimeout("action()",speed);}action();
	  </script>

    <!-- Vendor css -->
    <link rel="stylesheet" href="{{ asset('assets/peserta2/src/vendors/@mdi/font/css/materialdesignicons.min.css')}}">

    <!-- Base css with customised bootstrap included -->
    <link rel="stylesheet" href="{{ asset('assets/peserta2/src/css/miri-ui-kit-free.css')}}">

    <!-- Stylesheet for demo page specific css -->
    <link rel="stylesheet" href="{{ asset('assets/peserta2/docs/assets/css/demo.css')}}">
</head>
<body class="login-page">

    @include('layout.auth.peserta.header')

    @yield('content')
    
    @include('layout.auth.peserta.footer')

    <script src="{{ asset('assets/peserta2/src/vendors/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/peserta2/src/vendors/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{ asset('assets/peserta2/src/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
</body>
</html>
