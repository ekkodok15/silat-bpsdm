@if(!empty(Auth::User()))
<header class="miri-ui-kit-header landing-header header-navbar-only">
@else
<header class="miri-ui-kit-header landing-header header-bg-2">
@endif
        <nav class="navbar navbar-expand-lg navbar-dark fixed-on-scroll">
            <div class="container">
            @if(!empty(Auth::User()))
            <a class="navbar-brand" href="{{route('home')}}" type="link"><img src="{{ asset('images/logo.png') }}" alt="logo"></a>
            @else
            <a class="navbar-brand" href="{{route('beranda')}}" type="link"><img src="{{ asset('images/logo.png') }}" alt="logo"></a>
            @endif
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#miriUiKitNavbar"
                    aria-controls="navbarSupportedContent2" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="mdi mdi-menu"></span>
                </button>

                <div class="collapse navbar-collapse" id="miriUiKitNavbar">
                    <div class="navbar-nav ml-auto">
                        <li class="nav-item">
                        @if(!empty(Auth::User()))
                        <a class="nav-link" href="{{route('home')}}" target="">Home</span></a>
                        @else
                        <a class="nav-link" href="{{route('beranda')}}" target="">Home</span></a>
                        @endif
                        </li>

                        @if(!empty(Auth::User()))
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                                Diklat
                            </a>
                            <div class="dropdown-menu dropdown-menu-right ">
                                <a href="{{route('diklat.all')}}" class="dropdown-item">Semua Diklat</a>
                                <a href="{{route('diklat.manajerial')}}" class="dropdown-item">Dikalt Manajerial</a>
                                <a href="{{route('diklat.fungsional')}}" class="dropdown-item">Diklat Fungsional</a>
                                <a href="{{route('diklat.teknis')}}" class="dropdown-item">Diklat Teknis</a>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{route('diklat.user.hostory')}}" target="">History Diklat</span></a>
                        </li>

                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                                {{Auth::User()->email}}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right ">
                                <a href="{{route('profile')}}" class="dropdown-item">Profile</a>
                                <a href="{{route('logout')}}" class="dropdown-item">logout</a>
                            </div>
                        </li>
                        @else
                        <li class="nav-item">
                            <a class="nav-link" href="https://bpsdm.gorontaloprov.go.id/">BPSDM</a>
                        </li>

                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                                Diklat
                            </a>
                            <div class="dropdown-menu dropdown-menu-right ">
                                <a href="#" class="dropdown-item">Dikalt Manajerial</a>
                                <a href="#" class="dropdown-item">Diklat Fungsional</a>
                                <a href="#" class="dropdown-item">Diklat Teknis</a>
                            </div>
                        </li>

                        <li class="nav-item">

                            <a class="nav-link nav-icon icon-fb" href="https://web.facebook.com/badan.diklat.gtlo"><i class="mdi mdi-facebook-box"></i></a>

                            <a class="nav-link nav-icon icon-insta" href="https://www.instagram.com/diklat_provgtlo/"><i class="mdi mdi-instagram"></i></a>

                            <a class="nav-link nav-icon icon-youtube" href="https://www.youtube.com/channel/UC14FBUakTMHiNpRTwd34rFg"><i class="mdi mdi-youtube"></i></a>

                        </li>
                        @endif

                        @if(!empty(Auth::User()))
                        @else
                        <form action="{{route('login')}}" class="form-inline ml-lg-3">
                            <button class="btn btn-danger">Login/Registrasi</button>
                        </form>
                        @endif
                    </div>
                </div>
            </div>
        </nav>
        <div
            class="miri-ui-kit-header-content text-center text-white d-flex flex-column justify-content-center align-items-center">
            @if(!empty(Auth::User()))
            <br>
            <br>
            <h1 class="display-3 text-white"></h1>
            <p class="h3 font-weight-light text-white">BPSDM PROVINSI GORONTALO</p>
            <p class="mt-4">
            </p>
            @else
            <h1 class="display-3 text-white">BPSDM PROVINSI GORONTALO</h1>
            <p class="h3 font-weight-light text-white">SINERGIS, INOVATIF, AKUNTABEL, PROFESIONAL</p>
            <p class="mt-4">
              <a href="{{route('login')}}" class="btn btn-danger btn-pill mr-2">Login Now</a>
              <a href="{{route('regist')}}" class="btn btn-success btn-pill">Signup Now</a>
            </p>
            @endif
        </div>
    </header>
