<footer class="pt-5 mt-2">
        <div class="container">
            <nav class="navbar navbar-light bg-transparent navbar-expand d-block d-sm-flex text-center">
                <span class="navbar-text">Copyright &copy; {{ now()->year }}  IT BPSDM GORONTALO PROVINSI</span>
                @if(!empty(Auth::User()))
                <div class="navbar-nav ml-auto justify-content-center">
                </div>
                @else
                <div class="navbar-nav ml-auto justify-content-center">
                    <a href="{{ route('login.admin')}}" class="nav-link">Administrator</a>
                </div>
                @endif
            </nav>
        </div>
    </footer>