@extends('layout.peserta.master')
@section('content')
    <section class="miri-ui-kit-section mt-5">
        <div class="container">
            <div class="d-md-flex justify-content-between row">

                <div class="feature-box px-3">
                    <span class="card-icon bg-danger text-white rounded-circle"><i class="mdi mdi-account-settings"></i></span>
                    <h3 class="mb-3">Diklat Manajerial</h3>
                    <p>Pelatihan manajerial merupakan salah satu sarana untuk mengurangi penurunan kualitas sumber daya manusianya dan berbagai organisasi.</p>
                </div>


                <div class="feature-box px-3">
                    <span class="card-icon bg-success text-white rounded-circle">
                      <i class="mdi mdi-set-left-right"></i></span>
                    <h3 class="mb-3">Diklat Fungsional</h3>
                    <p>Diklat Fungsional adalah diklat yang dilaksanakan untuk mencapai persyaratan kompetensi yang sesuai dengan jenjang jabatan fungsional.</p>
                </div>


                <div class="feature-box px-3">
                    <span class="card-icon bg-primary text-white rounded-circle"><i class="mdi mdi-settings-outline"></i></span>
                    <h3 class="mb-3">Diklat Teknis</h3>
                    <p>Diklat Teknis adalah Diklat yang dilaksanakan untuk memberikan pengetahuan dan/atau penguasaan ketrampilan pada suatu bidang tugas.</p>
                </div>

            </div>
        </div>
    </section>
    <section class="miri-ui-kit-section pricing-section">
        <div class="container">
            <h2>Diklat Bulan {{ now()->format('F') }}</h2>
            <p class="mb-5">Kumpulan diklat yang akan di selenggarakan bulan {{ now()->format('F') }}.</p>
            <div class="card-group">
                @foreach($training as $index => $result)
                <div class="card text-center">
                    <div class="card-body p-5">
                        <h4>{{ $result->name_training_plan }}</h4>
                        <p>Tanggal Pelaksanaan: {{ date('d-M-Y', strtotime($result->approved_start_date)) }} s/d {{ date('d-M-Y', strtotime($result->proposed_completion_date)) }}</p>
                        <p>
                            @if(date('Y-m-d', strtotime($result->regist_start_date)) <= now() && now() <= date('Y-m-d', strtotime($result->regist_completion_date."+1 days")))
                            @if(Auth::User() == null)
                            <form action="{{ route('login')}}" method="get">
                            @elseif(Auth::User() != null)
                            <form action="{{ route('diklat.user.pilih'.$result->id_training_plan)}}" method="get">
                            @endif
                                <button type="submit" class="btn btn-primary">Join Now</button>
                            </form>
                            @endif
                        </p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
