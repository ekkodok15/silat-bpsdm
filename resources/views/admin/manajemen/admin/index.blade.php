@extends('layout.admin.master')
@section('content')
    
      <div class="row">
        <div class="col-12">
          <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
              <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                <div class="col-6 d-flex align-items-center">
                  <h6 class="text-white text-capitalize ps-4 col-12">Manajemen Admin</h6>
                  <div class="col-11 text-end">
                    <a class="btn bg-gradient-dark mb-0" href="{{ route('manajemen.admin.add')}}"><i class="material-icons text-sm">add</i>&nbsp;&nbsp;Add New Admin</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-body px-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-center text-secondary text-xxs font-weight-bolder opacity-7" width="20px">#</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Email</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nama</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Unit Kerja</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Status</th>
                      <th class="text-secondary opacity-7">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($admin as $index => $result)
                    <tr>
                      <td class="align-middle text-center text-sm">
                      {{ $index+1 }}
                      </td>
                      <td>
                        {{$result->email}}
                      </td>
                      <td>
                        {{$result->name}}
                      </td>
                      <td>
                        {{$result->workUnitAdmin->work_unit}}
                      </td>
                      <td class="align-middle text-center text-sm">
                        @if($result->password == null)
                        not activate
                        @else
                        @if($result->status == 1)
                        <form action="{{ route('updateStatusAdmin') }}" method="post">
                          @csrf
                          <input type="hidden" id="id" name="id" value="{{$result->id_admin}}">
                          <input type="hidden" id="status" name="status" value="0">
                          <button type="submit" class="badge badge-sm bg-gradient-success"><span>Enable</span></button>
                        </form>
                        @elseif($result->status == 0)
                        <form action="{{ route('updateStatusAdmin') }}" method="post">
                          @csrf
                          <input type="hidden" id="id" name="id" value="{{$result->id_admin}}">
                          <input type="hidden" id="status" name="status" value="1">
                          <button type="submit" class="badge badge-sm bg-gradient-secondary"><span>Disable</span></button>
                        </form>
                        @endif
                        @endif
                      </td>
                      <td class="align-middle">
                        <a href="{{ route('manajemen.admin.update',$result->id_admin) }}" >
                          <button class="text-secondary font-weight-bold text-xs border-0" data-toggle="tooltip" data-original-title="Edit user">
                          Edit
                          </button>
                        </a>&nbsp;&nbsp;|&nbsp;&nbsp;
                        <form action="{{ route('delete.admin') }}" method="post" class="d-inline">
                          @csrf
                          <input type="hidden" name="id_admin" id="id_admin" value="{{$result->id_admin}}">
                          <button type="submit" onclick="return confirm('Apakah Anda Yakin Menghapus Data?');"  class="text-danger font-weight-bold text-xs border-0" background-color="white" data-toggle="tooltip" data-original-title="Hapus user">
                            Hapus
                          </button>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection