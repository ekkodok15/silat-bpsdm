@extends('layout.admin.master')
@section('content')
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="card mt-4">
            <div class="card-header p-3">
              <h5 class="mb-0"><i class="material-icons text-sm">add</i>&nbsp;&nbsp;Add New Admin</h5>
            </div>
            <div class="card-body p-4 pb-0">
              <form action="{{ route('createNewAdmin') }}" method="post">
                @csrf
                    <div class="input-group input-group-outline mb-3">
                      <label class="form-label">Email</label>
                      <input type="email" class="form-control" id="email" name="email">
                    </div>
                    <div class="input-group input-group-outline mb-3">
                      <label class="form-label">Nama</label>
                      <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="input-group input-group-outline mb-3">
                      <label class="form-label">Username</label>
                      <input type="text" class="form-control" id="username" name="username">
                    </div>
                    <div class="input-group input-group-static mb-3">
                      <label for="instansi" class="ms-0">Instansi</label>
                      <select class="form-control" id="instansi" name="instansi">
                      @foreach($instansi as $i)
                        <option value="{{$i->id_agency}}">&ensp;&ensp;{{$i->id_agency}}.&ensp;&ensp;{{$i->agency}}</option>
                      @endforeach
                      </select>
                    </div>
                    <div class="input-group input-group-static mb-3">
                      <label for="unitKerja" class="ms-0">Unit Kerja</label>
                      <select class="form-control" id="unitKerja" name="unitKerja">
                        @foreach($unitKerja as $uk)
                        <option value="{{$uk->id_work_unit}}">&ensp;&ensp;{{$uk->id_work_unit}}.&ensp;&ensp;{{$uk->work_unit}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="input-group input-group-static mb-3">
                      <label for="unitKlevelAdminerja" class="ms-0">Unit Kerja</label>
                      <select class="form-control" id="levelAdmin" name="levelAdmin">
                      @foreach($levelAdmin as $la)
                        <option value="{{$la->id_level_admin}}">&ensp;&ensp;&ensp;&ensp;{{$la->level_admin}}</option>
                      @endforeach
                      </select>
                    </div>
                    <div class="text-center">
                      <button type="submit" class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0">Create New Admin</button>
                    </div>
                </form>
                <br>
            </div>
          </div>
        </div>
      </div>
@endsection