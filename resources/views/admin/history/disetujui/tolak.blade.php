@extends('layout.admin.master')
@section('content')

      <div class="row">
        <div class="col-12">
          <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
              <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                <div class="col-6 d-flex align-items-center">
                  <h6 class="text-white text-capitalize ps-4 col-12">Tolak</h6>
                </div>
              </div>
            </div>
            <div class="card-body px-0 pb-2">
              <div class="card-body p-4 pb-0">
                <form action="{{ route('participantDisapproveProses') }}" method="post">
                  @csrf
                      <div class="input-group input-group-outline mb-3">
                        <label class="form-label">Deskripsi</label>
                        <input type="hidden" class="form-control" id="id" name="id" value="{{ $trainingparticipan->id_training_option }}">
                        <input type="text" class="form-control" id="deskripsi" name="deskripsi">
                      </div>
                      <div class="text-center">
                        <button type="submit" class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0">Disapprove This Participant</button>
                      </div>
                  </form>
                  <br>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
