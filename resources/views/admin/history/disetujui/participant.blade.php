@extends('layout.admin.master')
@section('content')

      <div class="row">
        <div class="col-12">
          <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
              <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                <div class="col-6 d-flex align-items-center">
                  <h6 class="text-white text-capitalize ps-4 col-12">Participant List</h6>
                  <div class="col-11 text-end">
                    <a class="btn bg-gradient-dark mb-0" href="{{ route('photoZip',$id) }}"> Export Photo</a>
                    <a class="btn bg-gradient-dark mb-0" href="{{ route('dataCsv',$id) }}" target="_blank"> Export CSV Data</a>
                  </div>
                </div>
              </div>
            </div>
            @if(!empty(session()->get( 'error' )))
                    <div class="alert alert-danger" role="alert">
                    {{ session()->get( 'error' ) }}
                    </div>
            @endif
            @if(!empty(session()->get( 'success' )))
                    <div class="alert alert-success" role="alert">
                        {{ session()->get( 'success' ) }}
                    </div>
            @endif
            <div class="card-body px-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-center text-secondary text-xxs font-weight-bolder opacity-7" width="20px">#</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">NIP/NRP/NPP/NIK</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nama</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Kode Kelas</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">No. Telfon</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Unit Kerja</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Photo Profile</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">SPT</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Rekomendasi Ruko</th>
                      <th class="text-secondary opacity-7">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($trainingparticipan as $index => $result)
                    <tr>
                      <td class="align-middle text-center text-sm">
                      {{ $index+1 }}
                      </td>
                      <td>
                      {{$result->nip_user}}
                      </td>
                      <td>
                      {{$result->takeUserBiodata->name}}
                      </td>
                      <td>
                      {{$result->takeClass->kd_class}}
                      </td>
                      <td>
                        {{$result->takeUserBiodata->phone}}
                      </td>
                      <td>
                        {{$result->takeUserBiodata->takeWorkUnit->work_unit}}
                      </td>
                      <td>
                        <div class="avatar avatar-xl position-relative">
                            <img src="{{ asset('/public/images').'/'.$result->takeUserBiodata->photo }}" alt="profile_image" class="w-100 border-radius-lg shadow-sm">
                          </div>
                      </td>
                      <td>
                        <a href="{{ asset('/public/spt').'/'.$result->spt}}" class="btn btn-link text-dark text-sm mb-0 px-0 ms-4" target="_blank">
                            <i class="material-icons text-lg position-relative me-1">picture_as_pdf</i> PDF
                        </a>
                      </td>
                      <td>
                        <a href="{{ asset('/public/ruko').'/'.$result->ruko}}" class="btn btn-link text-dark text-sm mb-0 px-0 ms-4" target="_blank">
                            <i class="material-icons text-lg position-relative me-1">picture_as_pdf</i> PDF
                        </a>
                      </td>
                      <td class="align-middle text-center text-sm">
                        @if($result->status == "0")
                        <a href="{{ route('diklat.disetujui.participant.approve',$result->id_training_option) }}" >
                            <button class="text-success font-weight-bold text-xs border-0" data-toggle="tooltip" data-original-title="Edit user">
                            Setujui
                            </button>
                        </a>
                        <a href="{{ route('diklat.disetujui.participant.disapprove.view',$result->id_training_option) }}" >
                            <button class="text-danger font-weight-bold text-xs border-0" data-toggle="tooltip" data-original-title="Edit user">
                            Tolak
                            </button>
                        </a>
                        @else
                        @endif
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
