@extends('layout.admin.master')
@section('content')
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="card mt-4">
            <div class="card-header p-3">
              <h5 class="mb-0"><i class="material-icons text-sm">add</i>&nbsp;&nbsp;{{$trainingplan->name_training_plan}}</h5>
            </div>
            <div class="card-body p-4 pb-0">
              <form action="{{ route('updateTrainingApproved',$trainingplan->id_training_plan) }}" method="post">
                @csrf
                    <div class="input-group input-group-outline mb-3 is-filled">
                      <label class="form-label">Trining</label>
                      <input type="text" class="form-control" id="training" name="training" value="{{$trainingplan->takeTraining->training}}" disabled>
                    </div>
                    <div class="input-group input-group-outline mb-3 is-filled">
                      <label class="form-label">Name Of Proposer</label>
                      <input type="text" class="form-control" id="admin" name="admin" value="{{$trainingplan->takeAdmin->name}}" disabled>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="input-group input-group-outline mb-3 is-filled">
                          <label class="form-label">Type Of Implementation</label>
                          <input type="text" class="form-control" id="typeOfImplementation" name="typeOfImplementation" value="{{$trainingplan->takeTypeOfImplementation->type_of_implementation}}" disabled>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="input-group input-group-outline mb-3 is-filled">
                          <label class="form-label">Implementation Pattern</label>
                          <input type="text" class="form-control" id="implementationPattern" name="implementationPattern" value="{{$trainingplan->takeImplementationPattern->implementation_pattern}}" disabled>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="input-group input-group-outline mb-3 is-filled">
                          <label class="form-label">Budget Source</label>
                          <input type="text" class="form-control" id="budgetSource" name="budgetSource" value="{{$trainingplan->takeBudgetSource->budget_source}}" disabled>
                        </div>
                      </div>
                    </div>
                    <div class="input-group input-group-outline mb-3 is-filled">
                      <label class="form-label">Location Of Training</label>
                      <input type="text" class="form-control" id="location" name="location" value="{{$trainingplan->training_location}}">
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group input-group-static mb-3">
                          <label>Approved Start Date Of Training</label>
                          <input type="date" class="form-control" id="tgl_mulai" name="setuju_tgl_mulai" value="{{ date('Y-m-d', strtotime($trainingplan->approved_start_date))}}">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group input-group-static mb-3">
                          <label>Approved End Date Of Training</label>
                          <input type="date" class="form-control" id="tgl_selesai" name="setuju_tgl_selesai" value="{{ date('Y-m-d', strtotime($trainingplan->approved_completion_date))}}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group input-group-static mb-3">
                          <label>Start Registrasi Date Of Training</label>
                          <input type="date" class="form-control" id="tgl_mulai" name="regist_tgl_mulai" value="{{ date('Y-m-d', strtotime($trainingplan->regist_start_date))}}">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group input-group-static mb-3">
                          <label>End Registrasi Date Of Training</label>
                          <input type="date" class="form-control" id="tgl_selesai" name="regist_tgl_selesai" value="{{ date('Y-m-d', strtotime($trainingplan->regist_completion_date))}}">
                        </div>
                      </div>
                    </div>
                    <div class="text-center">
                      <button type="submit" class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0">Approve This Approved Training</button>
                    </div>
                </form>
                <br>
            </div>
          </div>
        </div>
      </div>
@endsection