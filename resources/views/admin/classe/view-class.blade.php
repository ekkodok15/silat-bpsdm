@extends('layout.admin.master')
@section('content')

      <div class="row">
        <div class="col-12">
          <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
              <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                <div class="col-6 d-flex align-items-center">
                  <h6 class="text-white text-capitalize ps-4 col-12">Kelas and Peserta</h6>
                </div>
              </div>
            </div>
            <div class="card-body px-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-center text-secondary text-xxs font-weight-bolder opacity-7" width="20px">#</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Kelas</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">NIP Peserta</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nama Peserta</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Email Peserta</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Tanggal Daftar</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Berkas</th>
                      <th class="text-secondary text-center opacity-7">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($trainingoption as $index => $result)
                    <tr>
                      <td class="align-middle text-center text-sm">
                      {{ $index+1 }}
                      </td>
                      <td>
                        {{$result->takeClass->name_class}}
                      </td>
                      <td>
                        {{$result->nip_user}}
                      </td>
                      <td>
                        {{ $result->takeUserBiodata->name }}
                      </td>
                      <td>
                        {{ $result->takeUserBiodata->takeUser->email }}
                      </td>
                      <td>
                        {{$result->created_at}}
                      </td>
                      <td>
                        Download
                      </td>
                      <td>
                        Aksi
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
