@extends('layout.admin.master')
@section('content')
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="card mt-4">
            <div class="card-header p-3">
              <h5 class="mb-0"><i class="material-icons text-sm">add</i>&nbsp;&nbsp;Add New Category</h5>
            </div>
            <div class="card-body p-4 pb-0">
              <form action="{{ route('createNewCategory') }}" method="post">
                @csrf
                    <div class="input-group input-group-outline mb-3">
                      <label class="form-label">Categori Type Of Training</label>
                      <input type="text" class="form-control" id="category" name="category">
                    </div>
                    <div class="text-center">
                      <button type="submit" class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0">Create New Category</button>
                    </div>
                </form>
                <br>
            </div>
          </div>
        </div>
      </div>
@endsection