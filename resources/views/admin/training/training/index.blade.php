@extends('layout.admin.master')
@section('content')
    
      <div class="row">
        <div class="col-12">
          <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
              <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                <div class="col-6 d-flex align-items-center">
                  <h6 class="text-white text-capitalize ps-4 col-12">Catagory Training</h6>
                  <div class="col-11 text-end">
                    <a class="btn bg-gradient-dark mb-0" href="{{ route('diklat.add')}}"><i class="material-icons text-sm">add</i>&nbsp;&nbsp;Add New Training</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-body px-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-center text-secondary text-xxs font-weight-bolder opacity-7" width="20px">#</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Diklat</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Kategori Jenis Diklat</th>
                      <th class="text-secondary opacity-7">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($training as $index => $result)
                    <tr>
                      <td class="align-middle text-center text-sm">
                      {{ $index+1 }}
                      </td>
                      <td>
                        {{$result->training}}
                      </td>
                      <td>
                        {{$result->typeTraining->type_of_training}}
                      </td>
                      <td class="align-middle">
                        <a href="{{ route('diklat.update',$result->id_training) }}" >
                          <button class="text-secondary font-weight-bold text-xs border-0" data-toggle="tooltip" data-original-title="Edit Category">
                          Edit
                          </button>
                        </a>&nbsp;&nbsp;|&nbsp;&nbsp;
                        <form action="{{ route('deleteTraining') }}" method="post" class="d-inline">
                          @csrf
                          <input type="hidden" name="id_training" id="id_training" value="{{$result->id_training}}">
                          <button type="submit" onclick="return confirm('Apakah Anda Yakin Menghapus Data?');"  class="text-danger font-weight-bold text-xs border-0" background-color="white" data-toggle="tooltip" data-original-title="Hapus Category">
                            Hapus
                          </button>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection