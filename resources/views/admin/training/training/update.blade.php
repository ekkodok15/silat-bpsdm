@extends('layout.admin.master')
@section('content')
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="card mt-4">
            <div class="card-header p-3">
              <h5 class="mb-0"><i class="material-icons text-sm">add</i>&nbsp;&nbsp;Add New Admin</h5>
            </div>
            <div class="card-body p-4 pb-0">
              <form action="{{ route('updateTraining',$training->id_training) }}" method="post">
                @csrf
                <div class="input-group input-group-outline mb-3 is-filled">
                      <label class="form-label">Training</label>
                      <input type="text" class="form-control" id="training" name="training" value="{{$training->training}}">
                    </div>
                    <div class="input-group input-group-static mb-3">
                      <label for="category" class="ms-0">Categori Type Of Training</label>
                      <select class="form-control" id="category" name="category">
                        @foreach($typeoftraining as $tot)
                          @if($training->type_of_training_id == $tot->id_type_of_training)
                          <option value="{{$tot->id_type_of_training}}" selected>&ensp;&ensp;{{$tot->id_type_of_training}}.&ensp;&ensp;{{$tot->type_of_training}}</option>
                          @else
                          <option value="{{$tot->id_type_of_training}}">&ensp;&ensp;{{$tot->id_type_of_training}}.&ensp;&ensp;{{$tot->type_of_training}}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                    <div class="text-center">
                      <button type="submit" class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0">Upadate This Data</button>
                    </div>
                </form>
                <br>
            </div>
          </div>
        </div>
      </div>
@endsection