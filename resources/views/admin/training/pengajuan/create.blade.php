@extends('layout.admin.master')
@section('content')
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="card mt-4">
            <div class="card-header p-3">
              <h5 class="mb-0"><i class="material-icons text-sm">add</i>&nbsp;&nbsp;Add New Training Plan</h5>
            </div>
            <div class="card-body p-4 pb-0">
              <form action="{{ route('createNewTrainingPlan') }}" method="post">
                @csrf
                      <div class="input-group input-group-outline mb-3">
                        <label class="form-label">Name Of Training</label>
                        <input type="text" class="form-control" id="name" name="name">
                      </div>
                    <div class="row">
                      <div class="input-group input-group-static mb-3">
                        <label for="category" class="ms-0">Training</label>
                        <select class="form-control" id="training" name="training">
                          @foreach($training as $t)
                          <option value="{{$t->id_training}}">&ensp;&ensp;{{$t->id_training}}.&ensp;&ensp;{{$t->training}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group input-group-static mb-1 col-md-6">
                          <label for="implementationPattern" class="ms-0">Pattern Of Training</label>
                          <select class="form-control" id="implementationPattern" name="implementationPattern">
                            @foreach($implementationPattern as $ip)
                            <option value="{{$ip->id_implementation_pattern}}">&ensp;&ensp;{{$ip->id_implementation_pattern}}.&ensp;&ensp;{{$ip->implementation_pattern}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group input-group-static mb-1 col-md-6">
                          <label for="budgetSource" class="ms-0">Budget Source Of Training</label>
                          <select class="form-control" id="budgetSource" name="budgetSource">
                            @foreach($budgetSource as $bs)
                            <option value="{{$bs->id_budget_source}}">&ensp;&ensp;{{$bs->id_budget_source}}.&ensp;&ensp;{{$bs->budget_source}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-group input-group-static mb-3">
                        <label for="typeOfImplementation" class="ms-0">Training</label>
                        <select class="form-control" id="typeOfImplementation" name="typeOfImplementation">
                          @foreach($typeOfImplementation as $toi)
                          <option value="{{$toi->id_type_of_implementation}}">&ensp;&ensp;{{$toi->id_type_of_implementation}}.&ensp;&ensp;{{$toi->type_of_implementation}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                      <div class="input-group input-group-outline mb-3">
                        <label class="form-label">Location Of Training</label>
                        <input type="text" class="form-control" id="location" name="location">
                      </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="input-group input-group-static mb-3">
                          <label>Start Date Of Training</label>
                          <input type="date" class="form-control" id="tgl_mulai" name="tgl_mulai">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group input-group-static mb-3">
                          <label>End Date Of Training</label>
                          <input type="date" class="form-control" id="tgl_selesai" name="tgl_selesai">
                        </div>
                      </div>
                    </div>
                    <div class="text-center">
                      <button type="submit" class="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0">Create New Training Plan</button>
                    </div>
                </form>
                <br>
            </div>
          </div>
        </div>
      </div>
@endsection