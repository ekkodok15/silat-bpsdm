@extends('layout.admin.master')
@section('content')
    
      <div class="row">
        <div class="col-12">
          <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
              <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                <div class="col-6 d-flex align-items-center">
                  <h6 class="text-white text-capitalize ps-4 col-12">Training Plans</h6>
                  <div class="col-11 text-end">
                    <a class="btn bg-gradient-dark mb-0" href="{{ route('pengusulan.diklat.add')}}"><i class="material-icons text-sm">add</i>&nbsp;&nbsp;Add New Training Plan</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-body px-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-center text-secondary text-xxs font-weight-bolder opacity-7" width="20px">#</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nama Diklat</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nama Admin Pengusul Diklat</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Pengajuan Tanggal Mulai</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Pengajuan Tanggal Selesai</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Status</th>
                      <th class="text-secondary text-center opacity-7">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($trainingPlan as $index => $result)
                    <tr>
                      <td class="align-middle text-center text-sm">
                      {{ $index+1 }}
                      </td>
                      <td>
                        {{$result->name_training_plan}}
                      </td>
                      <td>
                        {{$result->takeAdmin->name}}
                      </td>
                      <td>
                        {{date('d-F-Y', strtotime($result->proposed_start_date)) }}
                      </td>
                      <td>
                        {{date('d-F-Y', strtotime($result->proposed_completion_date)) }}
                      </td>
                      <td>
                        {{$result->takeStatus->status_training}}
                      </td>
                      <td class="align-middle text-center text-sm">
                        @if($result->status == 3)
                        @else
                        <a href="{{ route('pengusulan.diklat.view',$result->id_training_plan) }}" >
                          <button class="text-secondary font-weight-bold text-xs border-0" data-toggle="tooltip" data-original-title="Edit Category">
                          View
                          </button>
                        </a>&ensp;&ensp;|&ensp;&ensp;
                        <a href="{{ route('pengusulan.diklat.update',$result->id_training_plan) }}" >
                          <button class="text-success font-weight-bold text-xs border-0" data-toggle="tooltip" data-original-title="Edit Category">
                          Approve
                          </button>
                        </a>&ensp;&ensp;|&ensp;&ensp;
                        <form action="{{ route('deleteTrainingPlan') }}" method="post" class="d-inline">
                          @csrf
                          <input type="hidden" name="id_training_plan" id="id_training_plan" value="{{$result->id_training_plan}}">
                          <button type="submit" onclick="return confirm('Apakah Anda Yakin Untuk Tidak Menyetujui Pengajuan Diklat Ini?');"  class="text-danger font-weight-bold text-xs border-0" background-color="white" data-toggle="tooltip" data-original-title="Hapus Category">
                            Not Approve
                          </button>
                        </form>
                        @endif
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection