@extends('layout.peserta.master')
@section('content')
    <section class="miri-ui-kit-section pricing-section">
        <div class="container">
            <h2>Diklat Manajerial</h2>
            <p class="mb-5">Kumpulan diklat manajerial yang akan di selenggarakan oleh BPSDM Provinsi Gorontalo</p>
            <div class="card-group">
                @foreach($training as $index => $result)
                <div class="card text-center">
                    <div class="card-body p-5">
                        <h4>{{ $result->name_training_plan }}</h4>
                        <p>Tanggal Pelaksanaan: {{ $result->approved_start_date }} s/d {{ $result->proposed_completion_date }}</p>
                        <p>
                            @if(date('Y-m-d', strtotime($result->regist_start_date)) <= now() && now() <= date('Y-m-d', strtotime($result->regist_completion_date."+1 days")))
                            @if(!empty(Auth::User()))
                            @if(!empty($trainingoption[0]))
                            @if(!empty($trainingoption[0]['training_plan_id'] == $result->id_training_plan))
                            @else
                            <form action="{{ route('diklat.user.pilih',$result->id_training_plan)}}" method="get">
                                <button type="submit" class="btn btn-primary">Join Now</button>
                            </form>
                            @endif
                            @else
                            <form action="{{ route('diklat.user.pilih',$result->id_training_plan)}}" method="get">
                                <button type="submit" class="btn btn-primary">Join Now</button>
                            </form>
                            @endif
                            @else
                            <form action="{{ route('login')}}" method="get">
                                <button type="submit" class="btn btn-primary">Join Now</button>
                            </form>
                            @endif
                            @endif
                        </p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
