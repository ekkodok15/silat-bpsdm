@extends('layout.peserta.master')
@section('content')
<div class="container">
        <section class="Profile-header text-center">
            <form action="{{ route('chooseDiklat') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div>
                <H1>{{ $training->name_training_plan }}</H1>
                <H4>{{ $training->approved_start_date }} s/d {{ $training->approved_completion_date }}</H4>
                </div>
                <br>
                <hr>
                <br>
                <div class="miri-ui-kit-demo mb-4 pb-1">
                    <div class="row">
                        <input type="hidden" class="form-control form-control-pill" id="id_training_plan" name="id_training_plan" value="{{ $training->id_training_plan }}">
                        <div class="form-group col-md-9">
                            <input type="file" id="spt" name="spt" style="display:none" onchange="document.getElementById('namespt').value=this.value">
                            <input type="text" class="form-control form-control-pill" id="namespt">
                        </div>
                        <div class="form-group col-md-2">
                            <input type="button" class="btn btn-danger btn-pill" value="Surat Perintah Tugas" onclick="document.getElementById('spt').click()">
                        </div>
                        @if (Auth::User()->level_user_id == 2)
                        <div class="form-group col-md-9">
                            <input type="file" id="ruko" name="ruko" style="display:none" onchange="document.getElementById('nameruko').value=this.value">
                            <input type="text" class="form-control form-control-pill" id="nameruko">
                        </div>
                        <div class="form-group col-md-2">
                            <input type="button" class="btn btn-danger btn-pill" value="  Rekomendasi Ruko  " onclick="document.getElementById('ruko').click()">
                        </div>
                        @endif
                        <div class="form-group col-md-12">
                            <select class="form-control form-control-pill" name="class" id="class">
                                @foreach($class as $c)
                                    <option value="{{$c->id_class}}">{{$c->name_class}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <span>
                    Nb:<br>
                    1. Harap Lengkapi data berikut sebaik-baik mungkin dikarenakan data berikut akan di jadikan patokan untuk pengajuan ke smartbangkom. <br>
                    2. File Harus berformat pdf. <br>
                    3. File Berukuran Maximal 2MB. <br>
                    @if (Auth::User()->level_user_id == 2)
                    4. Rekomendasi Ruko didapatkan melalui <a href="https://ruko-pns.gorontaloprov.go.id/">Rumah Kompetensi</a>
                    @endif
                </span>
                <br>
                <br>
                <br>
                <Button type="submit" class="btn btn-block btn-primary">Simpan</Button>
            </form>
        </section>
    </div>
@endsection
