@extends('layout.peserta.master')
@section('content')
<div class="container">
        <section class="Profile-header text-center">
            <form action="{{ route('updateProfile') }}" method="post">
                @csrf
                @if($user->photo == null)
                <div>
                    <a href="{{ route('upload.photo') }}">
                        <image type="image" src="{{ asset('assets/peserta2/docs/assets/images/input-photo.png') }}" alt="profile" class="img-rounded mb-3" width="75" height="75">
                    </a>
                </div>
                @else
                <div>
                    <a href="{{ route('upload.photo') }}">
                        <image type="image" src="{{ asset('public/images/')."/".$user->photo }}" alt="profile" class="img-rounded mb-3" width="75" height="75">
                    </a>
                </div>
                @endif
                <div class="miri-ui-kit-demo mb-4 pb-1">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <input type="hidden" class="form-control form-control-pill" id="id_user" name="id_user" value="{{ $user->takeUser->id_user }}">
                            <input type="text" class="form-control form-control-pill" id="nip" name="nip" placeholder="NIP/NRP/NPP/NIK" value="{{ $user->nip }}" disabled>
                        </div>
                        <div class="form-group col-md-12">
                            <input type="text" class="form-control form-control-pill" id="name" name="name" placeholder="Nama Lengkap" value="{{ $user->name }}" required>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="text" class="form-control form-control-pill" id="tempat_lahir" name="tempat_lahir" placeholder="Tempat Lahir" value="{{ $user->place_of_birth }}" required>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="date" class="form-control form-control-pill" id="tanggal_lahir" name="tanggal_lahir" placeholder="Tanggal Lahir" value="{{ date('Y-m-d', strtotime($user->birth_day)) }}" required >
                        </div>
                        <div class="form-group col-md-6">
                            <select class="form-control form-control-pill" name="agama" id="agama">
                                @foreach($religion as $r)
                                    @if($user->religion_id == $r->id_religion)
                                    <option value="{{$r->id_religion}}" selected>{{$r->religion}}</option>
                                    @else
                                    <option value="{{$r->id_religion}}">{{$r->religion}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <select class="form-control form-control-pill" name="jenis_kelamin" id="jenis_kelamin">
                                @foreach($gender as $g)
                                    @if($user->gender_id == $g->id_gender)
                                    <option value="{{$g->id_gender}}" selected>{{$g->gender}}</option>
                                    @else
                                    <option value="{{$g->id_gender}}">{{$g->gender}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <input type="text" class="form-control form-control-pill" id="email" name="email" placeholder="Email" disabled value="{{ $user->takeUser->email }}">
                        </div>
                        <div class="form-group col-md-12">
                            <input type="text" class="form-control form-control-pill" id="phone" name="phone" placeholder="Nomer Telfon" value="{{ $user->phone }}" required>
                        </div>
                        <div class="form-group col-md-12">
                            <select class="form-control form-control-pill" name="jenis_pegawai" id="jenis_pegawai">
                                @foreach($participantype as $p)
                                    @if($user->participant_type_id == $p->id_participant_type)
                                    <option value="{{$p->id_participant_type}}" selected>{{$p->participant_type}}</option>
                                    @else
                                    <option value="{{$p->id_participant_type}}">{{$p->participant_type}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <select class="form-control form-control-pill" name="pangkat" id="pangkat">
                                @foreach($group as $gp)
                                    @if($user->group_id == $gp->id_group)
                                    <option value="{{$gp->id_group}}" selected>{{$gp->group}}</option>
                                    @else
                                    <option value="{{$gp->id_group}}">{{$gp->group}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <select class="form-control form-control-pill" name="golongan" id="golongan">
                                @foreach($rank as $rk)
                                    @if($user->rank_id == $rk->id_rank)
                                    <option value="{{$rk->id_rank}}" selected>{{$rk->rank}}</option>
                                    @else
                                    <option value="{{$rk->id_rank}}">{{$rk->rank}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <input type="text" class="form-control form-control-pill" id="jabatan" name="jabatan" placeholder="Jabatan Saat Ini" value="{{ $user->department }}" required>
                        </div>
                        <div class="form-group col-md-12">
                            <select class="form-control form-control-pill" name="instansi" id="instansi">
                                @foreach($agency as $a)
                                    @if($user->agency_id == $a->id_agency)
                                    <option value="{{$a->id_agency}}" selected>{{$a->agency}}</option>
                                    @else
                                    <option value="{{$a->id_agency}}">{{$a->agency}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <select class="form-control form-control-pill" name="unit_kerja" id="unit_kerja">
                                @foreach($workunit as $wk)
                                    @if($user->work_unit_id == $wk->id_work_unit)
                                    <option value="{{$wk->id_work_unit}}" selected>{{$wk->work_unit}}</option>
                                    @else
                                    <option value="{{$wk->id_work_unit}}">{{$wk->work_unit}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <input type="text" class="form-control form-control-pill" id="alamat_unit_kerja" name="alamat_unit_kerja" placeholder="Alamat Unit Kerja" value="{{ $user->work_unit_address }}" required>
                        </div>
                    </div>
                </div>
                <span>Nb. Harap Lengkapi data berikut sebaik-baik mungkin dikarenakan data berikut akan di jadikan patokan untuk pengajuan ke smartbangkom.</span>
                <br>
                <br>
                <br>
                <Button type="submit" class="btn btn-block btn-primary">Simpan</Button>
            </form>
        </section>
    </div>
@endsection
