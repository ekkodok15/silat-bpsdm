@extends('layout.peserta.master')
@section('content')
    <section class="miri-ui-kit-section pricing-section">
        <div class="container">
          <br><br><br>
          <br><br>
            <h2>Apakah anda merupakan ASN Provinsi Gorontalo?</h2>
            <br><br>
            <div class="card-group">
                <div class="card text-center">
                    <div class="card-body p-5">
                        <p>
                        <form action="{{route('updateAsn')}}" method="post">
                        @csrf
                            <input type="hidden" name="status" id="status" value="2">
                            <button type="submit" class="btn btn-primary">&ensp;&ensp;&ensp;Ya&ensp;&ensp;&ensp;</button>
                        </form>
                        </p>
                    </div>
                </div>
                <div class="card text-center">
                    <div class="card-body p-5">
                        <p>
                        <form action="{{route('updateAsn')}}" method="post">
                        @csrf
                            <input type="hidden" name="status" id="status" value="3">
                            <button class="btn btn-danger">&ensp;Tidak&ensp;</button>
                        </form>
                        </p>
                    </div>
                </div>
            </div>
          <br><br>
          <br><br><br>
        </div>
    </section>
@endsection