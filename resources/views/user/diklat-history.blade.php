@extends('layout.peserta.master')
@section('content')
    <section class="miri-ui-kit-section pricing-section">
        <div class="container">
            <h2>Riwayat Pengambilan Diklat</h2>
            <div class="card-group">
                <table class="table align-items-center mb-0">
                    <thead>
                        <tr>
                            <th class="text-center text-secondary text-xxs font-weight-bolder opacity-7">No.</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nama Diklat</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Tanggal Mulai</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Tanggal Selesai</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Status</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($training as $index => $result)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>{{ $result->takeTrainingPlan->name_training_plan }}</td>
                            <td>{{ $result->takeTrainingPlan->approved_start_date }}</td>
                            <td>{{ $result->takeTrainingPlan->approved_completion_date }}</td>
                            @if ($result->takeTrainingPlan->status == 2)
                                @if ($result->status == "0")
                                <td>Dalam Proses Pendaftaran</td>
                                @elseif($result->status == "1")
                                <td>Berhasil Melakukan Pendaftaran</td>
                                @else
                                <td>Pendaftaran Anda Di Tolak Harap Periksa Kembali Data Diri dan Kelengkapan Berkas</td>
                                @endif
                            @elseif($result->takeTrainingPlan->status == 4)
                                <td>Dalam Masa Pelatihan</td>
                            @elseif($result->takeTrainingPlan->status == 4)
                                <td>Pelatihan Terselesaikan</td>
                            @endif
                            <td>{{ $result->deskripsi }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection
