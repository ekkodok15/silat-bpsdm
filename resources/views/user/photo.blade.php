@extends('layout.peserta.master')
@section('content')
<div class="container">
        <section class="Profile-header text-center">
            <div class="miri-ui-kit-demo mb-4 pb-1">
                <div class="row">
                    <div class="form-group col-md-12">
                        <img src="{{ asset('images/pasfoto.PNG') }}" alt="Contoh Pasfoto">
                    </div>
                </div>
            </div>
            <form action="{{ route('updatePhoto') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="miri-ui-kit-demo mb-4 pb-1">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="image"> Upload Pasfoto disini :  &ensp;&ensp;&ensp;</label>
                            <input type="file" id="image" name="image" placeholder="Profile Picture">
                        </div>
                    </div>
                </div>
                <span>Nb: <br>1. Harap Menambahkan foto formal berlatar belakang merah untuk kebutuhan data pelatihan diklat.
                <br>2. Pas Foto Berukuran 4 X 6
                <br>3. Foto Berasio 472 (Empat Ratus Tujuh Puluh Dua) pixcel X 709 (Tujuh Ratus Sembilan) Pixcel
                <br>4. Pria/Wanita Mengenakan Pakaian Bernuansa Putih Dengan Dasi Berwarna Hitam
                <br>5. Pria dapat mengenakan Jas Berwarna Hitam
                <br>6. Apabila Wanita Berjilbab Pendek, Mengenakan Jilbab Berwarna Putih Dengan Dasi Berwarna Hitam.
                <br>7. Apabila Wanita Berjilbab Panjang, Mengenakan Jilbab Berwarna Putih Tanpa Dasi.</span>
                <br>8. File Berformat jpg,jpeg.</span>
                <br>9. Pas Foto Yang DiUpload Merupakan File Asli Dan Bukan Bentuk Fisik Yang Di Foto Kembali.</span>
                <br>
                <br>
                <br>
                <Button type="submit" class="btn btn-block btn-primary">Simpan</Button>
            </form>
        </section>
    </div>
@endsection
