<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ReligionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $religions = [
            [
                'religion' => "Agama",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'religion' => "Islam",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'religion' => "Kristen Protestan",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'religion' => "Hindu",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'religion' => "Budha",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'religion' => "Kristen Katolik",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
        ];

        \DB::table('religions')->insert($religions);
    }
}
