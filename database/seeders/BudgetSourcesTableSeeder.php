<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BudgetSourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $budgetsources = [
            [
                'budget_source' => "APBN",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'budget_source' => "APBD",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'budget_source' => "KONTRIBUSI",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'budget_source' => "KERJASAMA",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
        ];

        \DB::table('budget_sources')->insert($budgetsources);
    }
}