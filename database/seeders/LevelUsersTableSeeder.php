<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LevelUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $levelusers = [
            [
                'level_user' => "STARTER",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'level_user' => "ASN PROV",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'level_user' => "NON-ASN PROV",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'level_user' => "Widyaiswara",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
        ];

        \DB::table('level_users')->insert($levelusers);
    }
}
