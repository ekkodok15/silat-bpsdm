<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AboutAppSeeder::class,
            AgenciesTableSeeder::class,
            BudgetSourcesTableSeeder::class,
            GendersTableSeeder::class,
            GroupsTableSeeder::class,
            ImplementationPatternsTableSeeder::class,
            LevelAdminsTableSeeder::class,
            LevelUsersTableSeeder::class,
            ParticipantTypesTableSeeder::class,
            RanksTableSeeder::class,
            ReligionsTableSeeder::class,
            StatusTrainingsTableSeeder::class,
            TypeOfImplementationsTableSeeder::class,
            TypeOfTrainingsTableSeeder::class,
            WorkUnitsTableSeeder::class,
            AdminSeeder::class,
            TrainingSeeder::class,
            BiodataUserSeeder::class,
            UserSeeder::class,
            UserAdiSeeder::class,
        ]);
    }
}
