<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserAdiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User;
        $admin->email                   = "iteksternal@gmail.com";
        $admin->password                = Hash::make("restapilms");
        $admin->level_user_id           = 1;
        $admin->nip_user                = "1010101010";
        $admin->status                  = "1";
        $admin->save();
    }
}
