<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class GendersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genders = [
            [
                'gender' => "Pria",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'gender' => "Wanita",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
        ];

        \DB::table('genders')->insert($genders);
    }
}