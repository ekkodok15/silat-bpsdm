<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = [
            [
                'group' => "Pangkat",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "Honorer",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "I/A",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "I/B",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "I/C",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "I/D",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "II/A",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "II/B",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "II/C",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "II/D",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "III/A",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "III/B",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "III/C",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "III/D",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "IV/A",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "IV/B",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "IV/C",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "IV/D",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "IV/E",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "IV/F",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "IV/G",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'group' => "IV/H",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
        ];

        \DB::table('groups')->insert($groups);
    }
}
