<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LevelAdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $leveladmins = [
            [
                'level_admin' => "Super Admin",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'level_admin' => "Admin Pengelola",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'level_admin' => "Admin Pengajuan Diklat",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
        ];

        \DB::table('level_admins')->insert($leveladmins);
    }
}
