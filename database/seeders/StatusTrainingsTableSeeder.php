<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StatusTrainingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statustrainings = [
            [
                'status_training' => "DI PROSES",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'status_training' => "DI SETUJUI",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'status_training' => "TIDAK DI SETUJUI",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'status_training' => "ON GOING",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'status_training' => "SELESAI",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
        ];

        \DB::table('status_trainings')->insert($statustrainings);
    }
}
