<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ranks = [
            [
                'rank' => "Golongan",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Honorer",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Juru Muda",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Juru Muda Tingkat I",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Juru",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Juru Tingkat I",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Pengatur Muda",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Pengatur Muda Tingkat I",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Pengatur",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Pengatur Tingkat I",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Penata Muda",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Penata Muda Tingkat I",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Penata",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Penata Tingkat I",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Pembina",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Pembina Tingkat I",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Pembina Utama Muda",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Pembina Utama Madya",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Pembina Utama",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Jenderal Polisi",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Komisaris Jenderal Polisi",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Inspektur Jenderal Polisi",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Brigadir Jenderal Polisi",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Komisaris Besar Polisi",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Ajun Komisaris Besar Polisi",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Komisaris Polisi",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Ajun Komisaris Polisi",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Inspektur Polisi Satu",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Inspektur Polisi Dua",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Ajun Inspektur Polisi Satu",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Ajun Inspektur Polisi Dua",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Brigadir Polisi Kepala",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Brigadir Polisi",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Brigadir Polisi Satu",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Brigadir Polisi Dua",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Ajun Brigadir Polisi",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Ajun Brigadir Polisi Satu",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Ajun Brigadir Polisi Dua",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Bhayangkara Kepala",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Bhayangkara Satu",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Bhayangkara Dua",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Yuana Darma TU",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Muda Darma TU",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Madya Darma TU",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Sena Darma TU",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Ajun Jaksa Madya",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Ajun Jaksa",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Jaksa Pratama",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Jaksa Muda",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Yuana Wira TU",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Muda Wira TU",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Madya Wira TU",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Sena Wira TU",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Jaksa Madya",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Jaksa Utama Pratama",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Jaksa Utama Muda",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Jaksa Utama Madya",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Adi Wira TU",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Letnan Dua",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Letnan Satu",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Kapten",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Mayor",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Kolonel",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Brigadir Jendral",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Mayor Jendral",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Letnan Jendral",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Jendral",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Letnan Kolonel",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Laksamana Pertama",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Laksamana Muda",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Laksamana Madya",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Laksamana",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Marsekal Pertama",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Marsekal Muda",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Marsekal Madya",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Marsekal",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Sersan Dua",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Sersan Kepala",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Sersan Satu",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Sersan Mayor",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Pembantu Letnan Dua",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Pembantu Letnan Satu",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Kopral Dua",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'rank' => "Kopral Satu",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
        ];

        \DB::table('ranks')->insert($ranks);
    }
}
