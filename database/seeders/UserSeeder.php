<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User;
        $admin->email                   = "eko.hidayat1515@gmail.com";
        $admin->password                = bcrypt("ekohidayat");
        $admin->level_user_id           = 1;
        $admin->nip_user                = "15091998";
        $admin->status                  = "1";
        $admin->remember_token          = Str::random(60);
        $admin->save();
    }
}
