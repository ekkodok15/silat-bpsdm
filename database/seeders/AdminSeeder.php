<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use App\Models\Admin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Admin;
        $admin->username        = "SuperAdmin";
        $admin->name            = "Super Admin BPSDM Provinsi Gorontalo";
        $admin->email           = "superadmin@bpsdm.gorontaloprov.go.id";
        $admin->password        = bcrypt("ekohidayat");
        $admin->status          = 1;
        $admin->remember_token  = Str::random(60);
        $admin->level_admin_id  = 1;
        $admin->agency_id       = 51;
        $admin->work_unit_id    = 27;
        $admin->save();
    }
}
