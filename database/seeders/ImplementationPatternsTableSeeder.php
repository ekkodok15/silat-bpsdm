<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ImplementationPatternsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $implementationpatterns = [
            [
                'implementation_pattern' => "Mandiri",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'implementation_pattern' => "Fasilitasi",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'implementation_pattern' => "Pengiriman/PNBP",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
        ];

        \DB::table('implementation_patterns')->insert($implementationpatterns);
    }
}