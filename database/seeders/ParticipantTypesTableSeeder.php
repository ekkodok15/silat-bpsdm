<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ParticipantTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $participanttypes = [
            [
                'participant_type' => "PNS",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'participant_type' => "Non-PNS",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'participant_type' => "POLRI",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'participant_type' => "TNI Angkatan Darat",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'participant_type' => "TNI Angkatan Laut",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'participant_type' => "TNI Angkatan Udara",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],

        ];

        \DB::table('participant_types')->insert($participanttypes);
    }
}
