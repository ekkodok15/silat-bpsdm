<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TypeOfImplementationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $typeofimplementations = [
            [
                'type_of_implementation' => "KLASIKAL",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'type_of_implementation' => "BLENDED",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
        ];

        \DB::table('type_of_implementations')->insert($typeofimplementations);
    }
}
