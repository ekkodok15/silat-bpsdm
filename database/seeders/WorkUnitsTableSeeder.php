<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class WorkUnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workunits = [
['work_unit' =>"UNIT KERJA",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"DINAS LINGKUNGAN HIDUP DAN KEHUTANAN",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"DINAS KOPERASI UKM PERINDAG",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"DINAS PEKERJAAN UMUM DAN PENATAAN RUANG",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"DINAS PERTANIAN",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"DINAS KELAUTAN & PERIKANAN",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"DINAS PERHUBUNGAN",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"DINAS PERPUSTAKAAN & KEARSIPAN",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"DINAS KOMINFO & STATISTIK",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"DINAS PMD, ADMINISTRASI KEPENDUDUKAN & CAPIL",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"DINAS PERUMAHAN RAKYAT & KAWASAN PERMUKIMAN",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"DINAS PANGAN",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"DINAS SOSIAL PP & PA",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"DINAS PNM, ESDM & NAKERTRANS",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"INSPEKTORAT",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SEKRETARIAT DPRD",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"BADAN PERENCANAAN PENELITIAN DAN PENGEMBANGAN DAERAH",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"BADAN PENGHUBUNG",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"BIRO UMUM",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"BIRO HUKUM",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"BIRO PEMERINTAHAN DAN KESRA",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"BIRO ORGANISASI",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"BIRO PENGADAAN",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"DINAS SATPOL PP, LINMAS & DAMKAR",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"DINAS KESEHATAN",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"BADAN PENANGGULANGAN BENCANA DAERAH",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"BIRO EKONOMI DAN PEMBANGUNAN",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"BADAN PENDIDIKAN & PELATIHAN",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"BADAN KESATUAN BANGSA & POLITIK",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"DINAS PENDIDIKAN, KEBUDAYAAN, PEMUDA DAN OLAHRAGA",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"DINAS PARIWISATA",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"BADAN KEPEGAWAIAN DAERAH",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"BADAN KEUANGAN",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Museum Purbakala",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Laboratorium Kesehatan Daerah",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD RSUD Dr. HASRI AINUN HABIBIE",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Balai Latihan Kerja",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Balai Pengujian dan Sertifikasi Mutu Barang",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Pengujian Material Jalan dan Bangunan",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Tempat Permrosesan Akhir Talumelito",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Kesatuan Pengelolaan Hutan Wilayah I dan II",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Kesatuan Pengelolaan Hutan Wilayah III",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Kesatuan Pengelolaan Hutan Wilayah IV",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Kesatuan Pengelolaan Hutan Wilayah V",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Kesatuan Pengelolaan Hutan Wilayah VI",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Kesatuan Pengelolaan Hutan Wilayah VII",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Balai Perlindungan Tanaman Pertanian",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Balai Latihan Teknis Pertanian",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Laboratorium Veteriner",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Balai Perbibitan Ternak",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Balai Pengawasan Mutu dan Keamanan Pangan",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Peikanan Budidaya Laut dan Payau",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Pelabuhan Perikanan Tenda",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD Pelabuhan Perikanan Gentuma",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPTD BPPMDPP",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SLBN Kota Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SLBN Kab. Gorontalo F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SLBN Paguyaman F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SLBN Tilamuta F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SLBN Marisa F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SLBN Bone Pantai F3",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SLBN Suwawa F3",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SLBN Gorontalo Utara F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 2 Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 3 Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 4 Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 5 Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Prasetya Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Muhammadiyah Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Talaga F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Telaga Biru F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Tilango F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Limboto F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 2 Limboto F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Limboto Barat F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Tibawa F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Pulubala F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Bongomeme F2 ",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Dungaliyo ** ",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Tolangohula  F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Boliyohutuo F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Asparaga F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Mootilango F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Biluhu F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Bilato F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Batudaa Pantai F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Muhamadiyah Batudaa F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Muhamadiyah Tolangohula F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 TIlamuta F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 2 Tilamuta F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Paguyaman F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 2 Paguyaman F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Botumoito F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Dulupi F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Wonosari F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 2 Wonosari F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Mananggu F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Paguyaman Pantai F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Paguat F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Marisa F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Buntulia F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Randangan F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Lemito F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Popayato F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Popayato Barat F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Tapa F3",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Kabila F3",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Suwawa F3",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Suwawa Timur F3",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Bonepantai Kab. Bone Bolango",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Bone Kab. Bone Bolango",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Bolango Ulu F3",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Pinogu F3",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 2 Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 3 Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 4 Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 5 Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Tirtayasa Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Muh. Kesehatan Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Tridharma Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Bina Taruna Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Limboto F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 2 Limboto F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Batudaa F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Dungalio F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Mootilango F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Pulubala F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Batudaa Pantai F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Almamater Telaga F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Gotong Royong Telaga F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Tek. Muh Limboto F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Pariwisata Bubohu F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Cendekia Boliyohuto F2",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Gorontalo Utara F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 2 Gorontalo Utara F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 3 Gorontalo Utara F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 4 Gorontalo Utara F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Taruna Bahari F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Kesehatan Gorontalo Utara F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Suwawa F3",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Bulango Selatan F3",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Bulango Utara F3",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Bonepantai F3",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Bone Raya F3",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri Model Gorontalo F1",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"UPT Perbenihan,pengadaan dan sertifikasi benih pertanian",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Boalemo F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Paguyaman F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 2 Paguyaman F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 3 Paguyaman F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Paguyaman Pantai F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Wonosari F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 2 Wonosari F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Mananggu F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Dulupi F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Botumoito F4",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Dengilo F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Marisa F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1  Duhiadaa F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Patilanggio F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Wanggarasi F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Taluditi F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Popayato F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Negeri 1 Popayato Timur F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Integral Hidayatullah Marisa F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Slafiyah Safiyah F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Kesehatan Randangan F6",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 1 Gorontalo Utara F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 2 Gorontalo Utara F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 3 Gorontalo Utara F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 4 Gorontalo Utara F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 5 Gorontalo Utara F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 6 Gorontalo Utara F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 7 Gorontalo Utara F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 8 Gorontalo Utara F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 9 Gorontalo Utara F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Negeri 10 Gorontalo Utara F5",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Islam Al-Akhyaar F5  ",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMA Terpadu Wirabakti F3",'created_at' => new \DateTime,'updated_at' => null,],
['work_unit' =>"SMK Muhammadiyah Utama Pohuwato F6",'created_at' => new \DateTime,'updated_at' => null,],
    ];

    \DB::table('work_units')->insert($workunits);
    }
}
