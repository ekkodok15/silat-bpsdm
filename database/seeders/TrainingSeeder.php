<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TrainingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trainings = [
                        [
                            'training' => "Pelatihan Menyusun Spesifikasi Teknik",
                            'type_of_training_id' => 2,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Penyusunan Harga Perkiraan ",
                            'type_of_training_id' => 2,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Dasar CPNS",
                            'type_of_training_id' => 1,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Pengelola Aset",
                            'type_of_training_id' => 3,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Kepemimpinan Administrator",
                            'type_of_training_id' => 1,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Kepemimpinan Administrator I",
                            'type_of_training_id' => 1,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Calon Penyuluh Anti Korupsi",
                            'type_of_training_id' => 2,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Dan Pencegahan Dan Pengendalian Infeksi Bagi Tenaga Di Fanyakes",
                            'type_of_training_id' => 2,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Bantuan Hidup Dasar (Nakes/Non Nakes)",
                            'type_of_training_id' => 3,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Latihan Dasar (Latsar)",
                            'type_of_training_id' => 1,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Pengadaan Barang Jasa Pemerintah Tingkat Dasar Dan Ujian Sertifikasi PBJ Tingkat Dasar",
                            'type_of_training_id' => 3,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Kepemimpinan Pengawasan",
                            'type_of_training_id' => 1,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Pengelolaan Keuangan Desa (APBDes)",
                            'type_of_training_id' => 2,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Penyusunan,Pelaksanaan,Pengendalian,Dan Aspek Hukum Kontrak ",
                            'type_of_training_id' => 2,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Pengadaan Barang/Jasa Pemerintah Tingkat Dasar",
                            'type_of_training_id' => 2,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Uji Sertifikasi Keahlian PBJ Tingakat Dasar",
                            'type_of_training_id' => 2,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Pengadaan Barang/Jasa Tingkat Dasar Oleh Pemerintah Desa",
                            'type_of_training_id' => 2,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Advance Cardio Life Support (Dokter/Perawat)",
                            'type_of_training_id' => 2,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Polisi Pamong Praja Bagi Tenaga Pengawas Dan Pelaksana",
                            'type_of_training_id' => 2,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Standar Kompetensi Dan Evaluasi Jabatan",
                            'type_of_training_id' => 3,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                        [
                            'training' => "Pelatihan Penyusunan Proses Bisnis Instansi Pemerintah",
                            'type_of_training_id' => 3,
                            'created_at' => new \DateTime,
                            'updated_at' => null,
                        ],
                    ];

                    \DB::table('trainings')->insert($trainings);
    }
}
