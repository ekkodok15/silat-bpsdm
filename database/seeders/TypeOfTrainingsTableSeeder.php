<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TypeOfTrainingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $typeoftrainings = [
            [
                'type_of_training' => "DIKLAT MANAJERIAL",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'type_of_training' => "DIKLAT TEKNIS",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
            [
                'type_of_training' => "DIKLAT FUNGSIONAL",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
        ];

        \DB::table('type_of_trainings')->insert($typeoftrainings);
    }
}
