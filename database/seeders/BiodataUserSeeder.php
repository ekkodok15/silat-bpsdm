<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\UserBiodata;

class BiodataUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new UserBiodata;
        $admin->nip                     = "15091998";
        $admin->name                    = "Eko Hidayat";
        $admin->gender_id               = 1;
        $admin->religion_id             = 1;
        $admin->place_of_birth          = "Sorong";
        $admin->birth_day               = '1998-09-15';
        $admin->phone                   = "082296956452";
        $admin->participant_type_id     = 2;
        $admin->group_id                = 1;
        $admin->rank_id                 = 1;
        $admin->agency_id               = 51;
        $admin->work_unit_id            = 27;
        $admin->photo                   = "default.jpg";
        $admin->save();
    }
}
