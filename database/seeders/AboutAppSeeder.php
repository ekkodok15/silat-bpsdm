<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AboutAppSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aboutapp = [
            [
                'id' => 1,
                'app_name' => "SiLat - BPSDM Provinsi Gorontalo",
                'logo_full' => "logo-full.png",
                'logo_mini' => "logo.png",
                'description' => "This is registration before starting a training.",
                'link_website' => "https://bpsdm.gorontaloprov.go.id/",
                'link_facebook' => "https://web.facebook.com/badan.diklat.gtlo",
                'link_twitter' => "https://bpsdm.gorontaloprov.go.id/",
                'link_instagram' => "https://www.instagram.com/diklat_provgtlo/",
                'link_youtube' => "https://www.youtube.com/channel/UC14FBUakTMHiNpRTwd34rFg",
                'created_at' => new \DateTime,
                'updated_at' => null,
            ],
        ];

        \DB::table('about_apps')->insert($aboutapp);
    }
}
