<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_plans', function (Blueprint $table) {
            $table->increments('id_training_plan');
            $table->unsignedInteger('admin_id')->nullable();
            $table->string('name_training_plan');
            $table->unsignedInteger('training_id')->nullable();
            $table->unsignedInteger('type_of_implementation_id')->nullable();
            $table->date('proposed_start_date');
            $table->date('proposed_completion_date');
            $table->unsignedInteger('status')->nullable();
            $table->date('approved_start_date')->nullable();
            $table->date('approved_completion_date')->nullable();
            $table->date('regist_start_date')->nullable();
            $table->date('regist_completion_date')->nullable();
            $table->unsignedInteger('implementation_pattern_id')->nullable();
            $table->unsignedInteger('budget_source_id')->nullable();
            $table->string('training_location');
            $table->timestamps();
            $table->foreign('admin_id')
                ->on('admins')
                ->references('id_admin')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('training_id')
                ->on('trainings')
                ->references('id_training')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('type_of_implementation_id')
                ->on('type_of_implementations')
                ->references('id_type_of_implementation')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('implementation_pattern_id')
                ->on('implementation_patterns')
                ->references('id_implementation_pattern')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('budget_source_id')
                ->on('budget_sources')
                ->references('id_budget_source')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('status')
                ->on('status_trainings')
                ->references('id_status_training')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_plans');
    }
}
