<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_options', function (Blueprint $table) {
            $table->increments('id_training_option');
            $table->string('nip_user', 20)->nullable()->references('nip')->on('tb_user_biodatas');
            $table->unsignedInteger('training_plan_id')->nullable();
            $table->unsignedInteger('class_id')->nullable();
            $table->string('spt');
            $table->string('ruko');
            $table->timestamps();
            $table->foreign('training_plan_id')
                ->on('training_plans')
                ->references('id_training_plan')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('class_id')
                ->on('training_classes')
                ->references('id_class')
                ->onUpdate('cascade')
                ->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_options');
    }
}
