<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id_admin');
            $table->string('username');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->enum('status', ['0','1']);
            $table->unsignedInteger('level_admin_id')->nullable();
            $table->unsignedInteger('agency_id')->nullable();
            $table->unsignedInteger('work_unit_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('level_admin_id')
                ->on('level_admins')
                ->references('id_level_admin')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('agency_id')
                ->on('agencies')
                ->references('id_agency')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('work_unit_id')
                ->on('work_units')
                ->references('id_work_unit')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
