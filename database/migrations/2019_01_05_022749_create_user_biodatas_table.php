<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserBiodatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_biodatas', function (Blueprint $table) {
            $table->string('nip', 20)->primary();
            $table->string('name');
            $table->unsignedInteger('gender_id')->nullable();
            $table->unsignedInteger('religion_id')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->date('birth_day')->nullable();
            $table->string('phone', 13)->nullable();
            $table->unsignedInteger('participant_type_id')->nullable();
            $table->unsignedInteger('group_id')->nullable();
            $table->unsignedInteger('rank_id')->nullable();
            $table->unsignedInteger('agency_id')->nullable();
            $table->unsignedInteger('work_unit_id')->nullable();
            $table->string('photo')->nullable();
            $table->timestamps();
            $table->foreign('gender_id')
                ->on('genders')
                ->references('id_gender')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('religion_id')
                ->on('religions')
                ->references('id_religion')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('participant_type_id')
                ->on('participant_types')
                ->references('id_participant_type')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('group_id')
                ->on('groups')
                ->references('id_group')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('rank_id')
                ->on('ranks')
                ->references('id_rank')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('agency_id')
                ->on('agencies')
                ->references('id_agency')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->foreign('work_unit_id')
                ->on('work_units')
                ->references('id_work_unit')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_biodatas');
    }
}
