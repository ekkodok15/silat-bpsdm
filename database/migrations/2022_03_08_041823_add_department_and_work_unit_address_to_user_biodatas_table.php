<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDepartmentAndWorkUnitAddressToUserBiodatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_biodatas', function (Blueprint $table) {
            $table->string('department')->nullable();
            $table->string('work_unit_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_biodatas', function (Blueprint $table) {
            $table->dropColumn('department');
            $table->dropColumn('work_unit_address');
        });
    }
}
