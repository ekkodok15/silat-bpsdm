<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_classes', function (Blueprint $table) {
            $table->increments('id_class');
            $table->string('name_class');
            $table->unsignedInteger('training_plan_id')->nullable();
            $table->string('kd_class');
            $table->string('area');
            $table->string('address');
            $table->integer('capacity');
            $table->timestamps();
            $table->foreign('training_plan_id')
                ->on('training_plans')
                ->references('id_training_plan')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_classes');
    }
}
